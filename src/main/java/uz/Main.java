package uz;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import uz.config.AppConfig;
import uz.config.PersistenceConfig;
import uz.controllers.events.EventBus;
import uz.controllers.events.FxEventBus;
import uz.utils.FxmlLoader;

import java.net.URL;
import java.util.ResourceBundle;

public class Main extends Application {
    public static AnnotationConfigApplicationContext ctx;
    public static EventBus eventBus = new FxEventBus();
    public  static  Stage window;
    public static boolean dayOrNight = false;
    @Override
    public void start(Stage primaryStage) throws Exception
    {
        Main.window = primaryStage;
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource(FxmlLoader.loading.loadScreen));
        primaryStage.setTitle("Сут махсулоти");
        primaryStage.setScene(new Scene(root));
        primaryStage.setMaximized(false);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.getIcons().add(new Image("/ico/tilloDomor.jpg"));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}

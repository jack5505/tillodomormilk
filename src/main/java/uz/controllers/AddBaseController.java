package uz.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import uz.Main;
import uz.controllers.controllerEvents.UpdateBaseEvent;
import uz.entities.TypeSeller;
import uz.service.SellerService;
import uz.utils.HelpUtils;

import java.net.URL;
import java.util.ResourceBundle;

public class AddBaseController implements Initializable
{
    @FXML
    private JFXButton btnAdd;
    @FXML
    private JFXButton btnCancel;
    @FXML
    private JFXTextField field;
    private TypeSeller typeSeller;
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        registerEvents();

    }

    private void registerEvents()
    {
        new HelpUtils().setNumeric(field);
        btnAdd.setOnAction(event ->
        {
            if(!field.getText().isEmpty())
            {
                Main.ctx.getBean(SellerService.class).updateBase(Long.parseLong(field.getText()),typeSeller);
                UpdateBaseEvent updateBaseEvent = new UpdateBaseEvent(UpdateBaseEvent.ANY);
                Main.eventBus.fireEvent(updateBaseEvent);
                close(event);
            }


        });
        btnCancel.setOnAction(event -> {
            close(event);
        });
    }

    private void close(ActionEvent event) {
        Stage stage = (Stage)((Button)(event.getSource())).getScene().getWindow();
        stage.close();
    }

    public void setTypeSeller(TypeSeller typeSeller) {
        this.typeSeller = typeSeller;
    }
}

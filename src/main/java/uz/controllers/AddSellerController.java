package uz.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import uz.Main;
import uz.controllers.controllerEvents.AddDataEvent;
import uz.controllers.tableData.SuppModel;
import uz.dto.SellerDto;
import uz.entities.TypeSeller;
import uz.service.SellerService;

import java.net.URL;
import java.util.ResourceBundle;

public class AddSellerController implements Initializable
{
    @FXML
    private JFXButton btnAdd;

    @FXML
    private JFXButton btnCancel;

    @FXML
    private JFXTextField fieldName;

    @FXML
    private JFXTextField fieldDirector;

    @FXML
    private JFXTextField fieldPhone;

    @FXML
    private JFXTextField fieldAddress;

    @FXML
    private Label require1;

    @FXML
    private Label info;

    @FXML
    private Label company;

    /*Anvar*/
    @FXML
    private JFXCheckBox aholiCheck;
    @FXML
    private JFXCheckBox fermerCheck;

    private TypeSeller typeSeller;

    private  SellerDto sellerDto = new SellerDto();

    public void initialize(URL location, ResourceBundle resources)
    {
        typeSeller=TypeSeller.fermer;
        fermerCheck.setSelected(true);
        prepareForTextFields();
        events();
    }

    private void events() {
            btnAdd.setOnAction(events ->
            {
                    require1.setVisible(fieldName.getText().isEmpty());
                    if(!require1.isVisible())
                    {
                            sellerDto.setCompanyName(fieldName.getText());
                            sellerDto.setAddress(fieldAddress.getText());
                            sellerDto.setDirector(fieldDirector.getText());
                            sellerDto.setPhone(fieldPhone.getText());
                            sellerDto.setSellerType(typeSeller);
                            Main.ctx.getBean(SellerService.class).saveSeller(sellerDto);
                            clearFields();
                            info.setVisible(true);
                            company.setVisible(true);
                            company.setText(sellerDto.getCompanyName());
                            AddDataEvent addDataEvent = new AddDataEvent(AddDataEvent.ANY);
                            Main.eventBus.fireEvent(addDataEvent);

                    }
            });
            btnCancel.setOnAction(event ->
            {
                Stage stage = (Stage) ((Button)(event.getSource())).getScene().getWindow();
                stage.close();
            });
            /*Anvar*/
            aholiCheck.setOnAction(event -> {
                if(fermerCheck.isSelected())
                    fermerCheck.setSelected(false);
                typeSeller=TypeSeller.aholi;
                System.out.println(typeSeller);
                fieldName.setText("Ахоли");
                fieldName.setEditable(false);

            });
            fermerCheck.setOnAction(event -> {
                if(aholiCheck.isSelected())
                    aholiCheck.setSelected(false);
                typeSeller=TypeSeller.fermer;
                clearFields();
                fieldName.setEditable(true);
            });


    }


    private void clearFields() {
        fieldName.setText("");
        fieldAddress.setText("");
        fieldDirector.setText("");
        fieldPhone.setText("");
    }

    private void prepareForTextFields() {
        require1.setVisible(false);
        info.setVisible(false);
        company.setVisible(false);
    }
    public void setForUpdate(SuppModel sellerDto){
        this.sellerDto.setId(sellerDto.getDbId());
        this.sellerDto.setCompanyName(sellerDto.getCompanyName());
        this.sellerDto.setPhone(sellerDto.getPhone());
        this.sellerDto.setDirector(sellerDto.getDirectorName());
        this.sellerDto.setAddress(sellerDto.getAddress());
        if(sellerDto.getCompanyName().equals("Ахоли")){
            this.sellerDto.setSellerType(TypeSeller.aholi);
            aholiCheck.setSelected(true);
            fieldName.setEditable(false);
            fermerCheck.setSelected(false);
        }
        fieldName.setText(this.sellerDto.getCompanyName());
        fieldPhone.setText(this.sellerDto.getPhone());
        fieldDirector.setText(this.sellerDto.getDirector());
        fieldAddress.setText(this.sellerDto.getAddress());
    }
}

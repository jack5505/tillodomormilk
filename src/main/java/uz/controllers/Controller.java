package uz.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.StageStyle;
import uz.Main;
import uz.controllers.controllerEvents.AddDataEvent;
import uz.controllers.controllerEvents.AddMoneyEvent;
import uz.controllers.controllerEvents.IncomDataEvent;
import uz.controllers.controllerEvents.PayMoneyEvent;
import uz.controllers.tableData.HistoryModel;
import uz.controllers.tableData.SuppModel;
import uz.entities.UserType;
import uz.service.HistoryService;
import uz.service.PayfundService;
import uz.service.SellerService;
import uz.utils.FxmlLoader;
import uz.utils.Window;

import java.math.BigDecimal;
import java.net.URL;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable
{
    @FXML
    private MenuItem addSupplyer;
    @FXML
    private MenuItem supplyerHistory;
    @FXML
    private MenuItem exit;
    @FXML
    private TableView<SuppModel> suppTable;
    @FXML
    private TableView<HistoryModel> historyTable;
    @FXML
    private TableColumn<SuppModel,String> suppName;
    @FXML
    private TableColumn<SuppModel,String>  suppDirector;
    @FXML
    private TableColumn<SuppModel,Long> num;
    @FXML
    private TableColumn<HistoryModel,Long> numH;
    @FXML
    private TableColumn<HistoryModel,String> milkH;
    @FXML
    private TableColumn<HistoryModel,String> milkCost;
    @FXML
    private TableColumn<HistoryModel,String> totalCostH;
    @FXML
    private TableColumn<HistoryModel,String> statusPaymentH;
    @FXML
    private TableColumn<HistoryModel,String> suppNameH;
    @FXML
    private TableColumn<HistoryModel,String> fatH;
    @FXML
    private TableColumn<HistoryModel,String> proteinH;
    @FXML
    private TableColumn<HistoryModel,String> saltH;
    @FXML
    private TableColumn<HistoryModel,String> waterH;
    @FXML
    private MenuItem supplyerDelet;
    @FXML
    private MenuItem editSupplyer;
    @FXML
    private Menu about;
    @FXML
    private MenuItem getMilk;
    @FXML
    private MenuItem transferMoney;
    @FXML
    private Label dayState;
    @FXML
    private MenuItem income;
    @FXML
    private MenuItem passChange;
    @FXML
    private MenuItem hisobot;
    @FXML
    private MenuItem kunHisobot;
    @FXML
    private MenuItem kechHisobot;

    @FXML
    private Label totalLitir1;
    @FXML
    private ImageView totalLitirImg;
    @FXML
    private JFXCheckBox show;
    @FXML
    private MenuItem incomeHistory;

    //Jami litiri to show
    @FXML
    private Label totalLitir;

    //Jami hisoblashilgan summa
    @FXML
    private Label payedSumma;

    //Jami pul o`tkazish yo`li bilan qarzga
    @FXML
    private Label debt;

    @FXML
    private TableColumn<HistoryModel,String> paidMoneyH;

    private BigDecimal totalLitirShow;

    private DecimalFormat format = new DecimalFormat("0.###");
    private Long totalPayedShow;

    private Long totalDebtShow;

    private Long cashToShow;
    @FXML
    private Label cashCasier;
    @FXML
    private Label payedSumma1;
    @FXML
    private ImageView payedSummaImg;
    @FXML
    private Label debt1;
    @FXML
    private ImageView debtImg;
    @FXML
    private Label cashCasier1;
    @FXML
    private ImageView cashCasierImg;
    @FXML
    private Label userName;

    private   UserType userType;

    @FXML
    private MenuItem addUsers;

    @FXML
    private MenuItem userList;

    private  Long id;
    public  UserType getUserType() {
        return userType;
    }
    private static Long totalMoney = (long)0;

    public  void setUserType(UserType userType,String name,Long id) {
        this.id = id;
        userType = userType;
        userName.setText(name);
        dayState.setText(userType.toString());
        if(userType.equals(UserType.Kunduzgi)){
            img.setImage(new Image("/img/icons8-солнце-50.png"));
        }
        if(userType.equals(UserType.Tungi)){
            img.setImage(new Image("/img/icons8-частичная-облачность-ночью-100.png"));
        }
    }

    /*Anvar*/
    @FXML
    private TextField search;
    @FXML
    private JFXButton getMilkBtn;
    @FXML
    private Label todaysDate;
    @FXML
    private ImageView img;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {



        todaysDate.setText(LocalDate.now().toString());
        registerEvents();
        tableRegister();
        fillData();
        show.setSelected(true);
    }
    private void fillData() {
        suppTable.getItems().addAll(Main.ctx.getBean(SellerService.class).getAllSuppData());
        historyTable.getItems().addAll(Main.ctx.getBean(HistoryService.class).todaysReport(Main.dayOrNight));
        totalLitirShow = Main.ctx.getBean(HistoryService.class).todayLitir();
        totalPayedShow = Main.ctx.getBean(HistoryService.class).todayPayed();
        totalDebtShow = Main.ctx.getBean(HistoryService.class).todayDebt();
        cashToShow = Main.ctx.getBean(PayfundService.class).getCashMoney(LocalDate.now().toString());
        totalLitir.setText(totalLitirShow.toString()+" Л");
        payedSumma.setText(totalPayedShow+"  сўм" );
        debt.setText(totalDebtShow+"  сўм");
        calculatTotalQassaMoney();
    }
    //Calculat total Money in Qassa
    private void calculatTotalQassaMoney() {
        cashCasier.setText(cashToShow - totalPayedShow +" сўм");
        totalMoney = Long.parseLong(cashToShow.toString()) - Long.parseLong(totalPayedShow.toString());
    }

    private void tableRegister() {
        num.setCellValueFactory(new PropertyValueFactory<SuppModel,Long>("id"));
        suppName.setCellValueFactory(new PropertyValueFactory<SuppModel,String>("companyName"));
        suppDirector.setCellValueFactory(new PropertyValueFactory<SuppModel,String>("directorName"));
        fatH.setCellValueFactory(new PropertyValueFactory<HistoryModel,String>("fatH"));
        proteinH.setCellValueFactory(new PropertyValueFactory<HistoryModel,String>("proteinH"));
        saltH.setCellValueFactory(new PropertyValueFactory<HistoryModel,String>("saltH"));
        waterH.setCellValueFactory(new PropertyValueFactory<HistoryModel,String>("waterH"));
        numH.setCellValueFactory(new PropertyValueFactory<HistoryModel,Long>("id"));
        milkH.setCellValueFactory(new PropertyValueFactory<HistoryModel,String>("totalLittr"));
        totalCostH.setCellValueFactory(new PropertyValueFactory<HistoryModel,String>("totalSumm"));
        paidMoneyH.setCellValueFactory(new PropertyValueFactory<HistoryModel,String>("paidMoney"));
        milkCost.setCellValueFactory(new PropertyValueFactory<HistoryModel,String>("milkCost"));
    //    statusPaymentH.setCellValueFactory(new PropertyValueFactory<HistoryModel,String>("statusPay"));
        suppNameH.setCellValueFactory(new PropertyValueFactory<HistoryModel,String>("fio"));
        suppTable.getStylesheets().add(getClass().getResource("/css/styles.css").toExternalForm());
        historyTable.getStylesheets().add(getClass().getResource("/css/styles.css").toExternalForm());
        statusPaymentH.setVisible(false);

    }

    private void registerEvents()
    {
        /*
        Kundizgi hisobot sut qabul qilganligi haqida
        day report about receiving milk
         */
        hisobot.setOnAction(event ->
        {
            Window window = new Window("Кунлик хисобот",FxmlLoader.mainScreen.hisobotScreen);
            HisobotController hisobotController = window.getController();
            window.setFullScreen(true);
            hisobotController.setAllOneDay();
            window.show();

        });
        kunHisobot.setOnAction(event ->
        {
            Window window = new Window("Кундизги хисобо",FxmlLoader.mainScreen.hisobotScreen);
            HisobotController hisobotController = window.getController();
            hisobotController.setDayOrNightReport(true);
            window.setFullScreen(true);
            window.show();

        });
        /*
        Kechki hisobot sut qabul qilinganliki haqida
        night report about receiving milk
         */
        kechHisobot.setOnAction(event ->
        {
            Window window = new Window("Кечки хисобот", FxmlLoader.mainScreen.hisobotScreen);
            HisobotController hisobotController = window.getController();
            hisobotController.setDayOrNightReport(false);
            window.setFullScreen(true);
            window.show();
        });
        //berini poqa olib qo`ydiq o`zlari o`zini tarihindaki daniya tushsin dayana qo`shmadiq
//        transferHistory.setOnAction(event -> {
//            Window window  = new Window("Тўловлар тарихи",FxmlLoader.milk.milkPayHistoryScreen);
//            HisobotController hisobotController = window.getController();
//            hisobotController.setDayOrNightReport(false);
//            window.setFullScreen(true);
//            window.show();
//        });
        suppTable.getItems().stream()
                .filter(item -> item.getId() == 1)
                .findAny()
                .ifPresent(item -> {
                    suppTable.getSelectionModel().select(item);
                    suppTable.scrollTo(item);
                });

//            suppTable.setOnMouseClicked(event -> {
//                System.out.println(suppTable.getSelectionModel().getSelectedItem().getCompanyName());
//            });

        //When clicked exit MenuItem here
        exit.setOnAction(event -> {
            closeWindow(event);
        });
        addSupplyer.setOnAction(event -> {
            Window window = new Window("Малумот кўшиш", FxmlLoader.mainScreen.add);
            window.setStageStyle(StageStyle.UTILITY);
            window.show();
        });
        editSupplyer.setOnAction(event ->
        {
            if(!suppTable.getSelectionModel().isEmpty())
            {
                Window window = new Window("Малумот кўшиш", FxmlLoader.mainScreen.add);
                window.setStageStyle(StageStyle.UTILITY);
                AddSellerController addSellerController = window.getController();
                addSellerController.setForUpdate(suppTable.getSelectionModel().getSelectedItem());
                window.show();
            }


        });
        //This eventBus when We add new data into db will register new eventBus
        Main.eventBus.addEventHandler(AddDataEvent.ANY,event -> {
            suppTable.getItems().clear();
            suppTable.getItems().addAll(Main.ctx.getBean(SellerService.class).getAllSuppData());
        });
        supplyerDelet.setOnAction(event ->
        {
            if(!suppTable.getSelectionModel().isEmpty()){
                Window window = new Window("Товар ўчириш",FxmlLoader.mainScreen.delet);
                window.setStageStyle(StageStyle.UTILITY);
                DeletController deletController = window.getController();
                deletController.setInfo(suppTable.getSelectionModel().getSelectedItem().getCompanyName(),suppTable.getSelectionModel().getSelectedItem().getDbId());
                window.show();
            }

        });
        about.setOnAction(event -> {
            System.out.println("here is clicked");
        });

        //call inputControllerForm
        getMilk.setOnAction(event -> {
            getMilkAction();
        });

        /*Anvar*/
        getMilkBtn.setOnAction(event -> {
            Window window  = new Window("Cут кабул килиш",FxmlLoader.mainScreen.input);
            window.setStageStyle(StageStyle.UTILITY);
            window.show();
        });
        search.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable,
                                String oldValue, String newValue) {
                change(newValue.toLowerCase());
            }
        });
        suppTable.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                    getMilkAction();
                }
            }
        });


        income.setOnAction(event -> {
            System.out.println("chiqsin");
            Window window = new Window("Кассага пул тулдириш",FxmlLoader.mainScreen.moneyIn);
            window.setStageStyle(StageStyle.UTILITY);
            window.show();
        });
        incomeHistory.setOnAction(event -> {
            Window window = new Window("Касса кабул килган пулар тарихи",FxmlLoader.mainScreen.historyMoney);
            window.setFullScreen(true);
            window.show();
        });

        //After adding new income Milk
        Main.eventBus.addEventHandler(IncomDataEvent.ANY,event -> {
            historyTable.getItems().clear();
            historyTable.getItems().addAll(Main.ctx.getBean(HistoryService.class).todaysReport(Main.dayOrNight));
            this.totalLitirShow = totalLitirShow.add(event.getUpdateData().getMilk());
            this.totalDebtShow += event.getUpdateData().getDebt();
            this.totalPayedShow += event.getUpdateData().getPayed();
            totalLitir.setText(totalLitirShow.toString()+" Л");
            payedSumma.setText(totalPayedShow+" сум");
            debt.setText(totalDebtShow+" сум");
            calculatTotalQassaMoney();

        });
        cashCasier.setOnMouseClicked(event -> {
            System.out.println(totalMoney);
        });
        supplyerHistory.setOnAction(event ->
        {
            if(!suppTable.getSelectionModel().isEmpty()){
                if(!suppTable.getSelectionModel().getSelectedItem().getCompanyName().equals("Ахоли")){
                    Window window = new Window("Таминотчи тарихи",FxmlLoader.mainScreen.historySeller);
                    HistorySellerController historySellerController = window.getController();
                    historySellerController.setTableToShow(suppTable.getSelectionModel().getSelectedItem());
                    window.setStageStyle(StageStyle.UNIFIED);
                    window.setFullScreen(true);
                    window.show();
                }
            }

        });
        transferMoney.setOnAction(event ->
        {
            Window window = new Window("Тўлов",FxmlLoader.transferScreen.payTransfer);
            if(!suppTable.getSelectionModel().isEmpty()){
                //selected supplyer here
                TransferMoneyController transferMoneyController = window.getController();
                transferMoneyController.setSupplyer(suppTable.getSelectionModel().getSelectedItem());
            }
            window.setStageStyle(StageStyle.UTILITY);
            window.show();


        });

        //After adding money
        Main.eventBus.addEventHandler(AddMoneyEvent.ANY,event -> {
            cashToShow = Main.ctx.getBean(PayfundService.class).getCashMoney(LocalDate.now().toString());
            calculatTotalQassaMoney();
        });
        show.setOnAction(event ->
        {
            if(show.isSelected()){
                showInfo(true);
            }
            else{
                showInfo(false);
            }
        });

        passChange.setOnAction(event -> {
            Window window = new Window("",FxmlLoader.mainScreen.passChangeScreen);
            window.setStageStyle(StageStyle.UTILITY);
            window.show();
        });

        //Foydalanuvchi qo`shish
        addUsers.setOnAction(event ->
        {
            if(this.id == 1)
            {
                Window window = new Window("",FxmlLoader.users.add);
                window.setStageStyle(StageStyle.UTILITY);
                window.show();
            }
        });
        //Foydalanuvchilar ro`yhati
        userList.setOnAction(event -> {
            if(this.id == 1)
            {
                Window window = new Window("",FxmlLoader.users.list);
                window.setStageStyle(StageStyle.UTILITY);
                window.show();
            }
        });
        Main.eventBus.addEventHandler(PayMoneyEvent.ANY,event -> {
            historyTable.getItems().clear();
            historyTable.getItems().addAll(Main.ctx.getBean(HistoryService.class).todaysReport(Main.dayOrNight));
            totalPayedShow = Main.ctx.getBean(HistoryService.class).todayPayed();
            cashToShow = Main.ctx.getBean(PayfundService.class).getCashMoney(LocalDate.now().toString());
            calculatTotalQassaMoney();
        });

    }




    /*Anvar*/
    private void change(String newValue) {
        List<SuppModel> list=Main.ctx.getBean(SellerService.class).getAllSuppData();
        List<SuppModel> filter=new ArrayList<>();
        for (int i = 0; i <list.size() ; i++) {
            if(list.get(i).getCompanyName().toLowerCase().contains(newValue) ||
                    list.get(i).getDirectorName().toLowerCase().contains(newValue) ||
                    list.get(i).getAddress().toLowerCase().contains(newValue) ||
                    list.get(i).getPhone().toLowerCase().contains(newValue)){
                filter.add(list.get(i));
            }
        }
        suppTable.getItems().clear();
        suppTable.getItems().addAll(filter);
    }
    private void getMilkAction(){
        Window window  = new Window("Cут кабул килиш",FxmlLoader.mainScreen.input);
        window.setStageStyle(StageStyle.UTILITY);
        if(suppTable.getSelectionModel().isEmpty()){
            window.show();
        }
        else {
            InputController inputController = window.getController();
            inputController.setMilkSupplyer(suppTable.getSelectionModel().getSelectedItem().getCompanyName(),
                    suppTable.getSelectionModel().getSelectedItem().getDbId(),
                    suppTable.getSelectionModel().getSelectedItem().getTypeSeller());
            window.show();
        }
    }


    private void closeWindow(ActionEvent event) {
        Platform.exit();
    }

    private void showInfo(boolean b) {
        totalLitir1.setVisible(b);
        totalLitir.setVisible(b);
        totalLitirImg.setVisible(b);
        payedSumma.setVisible(b);
        payedSumma1.setVisible(b);
        payedSummaImg.setVisible(b);
        debt.setVisible(b);
        debt1.setVisible(b);
        debtImg.setVisible(b);
        cashCasier.setVisible(b);
        cashCasier1.setVisible(b);
        cashCasierImg.setVisible(b);
    }

}

package uz.controllers;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import uz.Main;
import uz.controllers.controllerEvents.AddDataEvent;
import uz.service.SellerService;

import java.net.URL;
import java.util.ResourceBundle;

public class DeletController implements Initializable
{
    @FXML
    private JFXButton btnAdd;
    @FXML
    private JFXButton btnCancel;
    @FXML
    private Label info;
    private Long id;
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        info.setText("");
        registerEvents();
    }

    private void registerEvents() {
            btnAdd.setOnAction(event ->
            {
                Main.ctx.getBean(SellerService.class).deletSeller(this.id);
                AddDataEvent addDataEvent = new AddDataEvent(AddDataEvent.ANY);
                Main.eventBus.fireEvent(addDataEvent);
                close(event);
            });
            btnCancel.setOnAction(event -> {
                close(event);
            });
    }

    private void close(ActionEvent event) {
        Stage stage = (Stage)((Button)event.getSource()).getScene().getWindow();
        stage.close();

    }


    public void setInfo(String info,Long id) {
        this.info.setText(info);
        this.id = id;
    }
}

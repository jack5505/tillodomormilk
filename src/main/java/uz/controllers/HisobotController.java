package uz.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import uz.Main;
import uz.controllers.excel.DayExcelGenerator;
import uz.controllers.tableData.HistoryModel;
import uz.service.HistoryService;

import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class HisobotController implements Initializable {
    @FXML
    private TableView<HistoryModel> table;
    @FXML
    private TableColumn<HistoryModel,Long> id;
    @FXML
    private TableColumn<HistoryModel,String> litir;
    @FXML
    private TableColumn<HistoryModel,String> sellName;
    @FXML
    private TableColumn<HistoryModel,String> sell;
    @FXML
    private TableColumn<HistoryModel,String> fat;
    @FXML
    private TableColumn<HistoryModel,String> protein;
    @FXML
    private TableColumn<HistoryModel,String> salt;
    @FXML
    private TableColumn<HistoryModel,String> water;
    @FXML
    private TableColumn<HistoryModel,String> totalPrice;
    @FXML
    private TableColumn<HistoryModel,String> sellDate;
    @FXML
    private TableColumn<HistoryModel,String> state;
    @FXML
    private TableColumn<HistoryModel,String> litrPrice;
    @FXML
    private TableColumn<HistoryModel,String> paidMoney;
    @FXML
    private TableColumn<HistoryModel,String> needToPay;
    @FXML
    private JFXDatePicker startDate;
    @FXML
    private JFXButton search;
    @FXML
    private Label totalLitir;
    @FXML
    private Label totalPaidSumma;
    @FXML
    private Label totalNeedToPay;
    @FXML
    private Label totalCost;
    @FXML
    private JFXButton exc;
    private String searchDate = "";
    private static BigDecimal allLitir = BigDecimal.ZERO;
    private static BigDecimal allPayed = BigDecimal.ZERO;
    private static BigDecimal allDebt = BigDecimal.ZERO;
    private static BigDecimal allSumma = BigDecimal.ZERO;
    private boolean dayOrNight;
    private int excelType = 3;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        id.setCellValueFactory(new PropertyValueFactory<HistoryModel,Long>("id"));
        sellName.setCellValueFactory(new PropertyValueFactory<HistoryModel,String>("suppName"));
        sell.setCellValueFactory(new PropertyValueFactory<HistoryModel,String>("fio"));
        litir.setCellValueFactory(new PropertyValueFactory<HistoryModel,String>("totalLittr"));
        fat.setCellValueFactory(new PropertyValueFactory<HistoryModel,String>("fatH"));
        protein.setCellValueFactory(new PropertyValueFactory<HistoryModel,String>("proteinH"));
        salt.setCellValueFactory(new PropertyValueFactory<HistoryModel,String>("saltH"));
        water.setCellValueFactory(new PropertyValueFactory<HistoryModel,String>("waterH"));
        totalPrice.setCellValueFactory(new PropertyValueFactory<HistoryModel,String>("totalSumm"));
        sellDate.setCellValueFactory(new PropertyValueFactory<HistoryModel,String>("purchaseDate"));
       // state.setCellValueFactory(new PropertyValueFactory<HistoryModel,String>("statusPay"));
        litrPrice.setCellValueFactory(new PropertyValueFactory<HistoryModel,String>("milkCost"));
        paidMoney.setCellValueFactory(new PropertyValueFactory<HistoryModel,String>("paidMoney"));
        needToPay.setCellValueFactory(new PropertyValueFactory<HistoryModel,String>("needToPay"));
        table.getStylesheets().add(getClass().getResource("/css/styles.css").toExternalForm());
        startDate.setStyle("-fx-font-size: 18");
        exc.setOnAction(event ->
        {
                //berda hamma danniylani excela generatsiya qilib baradi lekin bazadan odelniy chaqirib
                //generatsiya qiladi
                 new DayExcelGenerator().generatedExcel(excelType,searchDate);
        });
        search.setOnAction(event -> {
            if(!startDate.getValue().toString().isEmpty())
            {
            table.getItems().clear();
            searchDate = startDate.getValue().toString();
            List<HistoryModel> getAll = Main.ctx.getBean(HistoryService.class).getAllHistoryByDate(searchDate);
            prepare();
            calculate(getAll);
            }
        });

    }
    // bu otchot kun yoki tunda ishliydi
    public void setDayOrNightReport(boolean dayOrNight){
         startDate.setVisible(false);
         search.setVisible(false);
         if(dayOrNight)
             excelType = 1;
         else
             excelType = 2;
         this.dayOrNight = dayOrNight;
         prepare();
         List<HistoryModel> getAll = Main.ctx.getBean(HistoryService.class).todaysReport(dayOrNight);
         calculate(getAll);
    }
    //bu hisobotga kirganda ishliydi default buguni kun bo`lib ishliydi
    public void setAllOneDay() {
        searchDate = LocalDate.now().toString();
        prepare();
        List<HistoryModel> getAll = Main.ctx.getBean(HistoryService.class).getAllHistoryByDate(searchDate);
        calculate(getAll);
    }
    private void prepare(){
        allLitir = BigDecimal.ZERO;
        allPayed = BigDecimal.ZERO;
        allDebt = BigDecimal.ZERO;
        allSumma = BigDecimal.ZERO;
    }
    private void calculate(List<HistoryModel> getAll){
        getAll.forEach(historyModel -> {
        allLitir = allLitir.add(new BigDecimal(historyModel.getTotalLittr()));
        allPayed = allPayed.add(new BigDecimal(historyModel.getPaidMoney()));
        allDebt = allDebt.add(new BigDecimal(historyModel.getNeedToPay()));
        allSumma = allSumma.add(new BigDecimal(historyModel.getTotalSumm()));
        table.getItems().add(historyModel);
    });
        totalLitir.setText(allLitir.toString());
        totalPaidSumma.setText(allPayed.toString());
        totalNeedToPay.setText(allDebt.toString());
        totalCost.setText(allSumma.toString());


    }
}




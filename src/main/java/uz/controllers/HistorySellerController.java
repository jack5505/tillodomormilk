package uz.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import uz.Main;
import uz.controllers.tableData.DataModel;
import uz.controllers.tableData.SuppModel;
import uz.service.HistoryService;
import uz.service.PayMilkService;


import java.math.BigDecimal;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

public class HistorySellerController implements Initializable
{
    @FXML
    private JFXDatePicker startDate;
    @FXML
    private JFXDatePicker endDate;
    @FXML
    private JFXButton search;
    @FXML
    private Label debt;
    @FXML
    private Label totalL;
    @FXML
    private Label totalP;
    @FXML
    private Label paid;
    @FXML
    private TableView<DataModel> table;
    @FXML
    private TableColumn<DataModel,Long> id;
    @FXML
    private TableColumn<DataModel,String> litir;
    @FXML
    private TableColumn<DataModel,String> fat;
    @FXML
    private TableColumn<DataModel,String> protein;
    @FXML
    private TableColumn<DataModel,String> salt;
    @FXML
    private TableColumn<DataModel,String> water;
    @FXML
    private TableColumn<DataModel,String> totalPrice;
    @FXML
    private TableColumn<DataModel,String> sellDate;
    @FXML
    private TableColumn<DataModel,String> state;
    @FXML
    private TableColumn<DataModel,String> litrPrice;
    @FXML
    private TableColumn<DataModel,String> paidMoney;
    @FXML
    private TableColumn<DataModel,String> needToPay;

    @FXML
    private Label fermaName;
    @FXML
    private Label sellerInfo;

    private Long sellerId;
    private SuppModel suppModel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
            registerTables();
            registerEvents();
            state.setVisible(false);
    }

    private void registerEvents() {
        search.setOnAction(event -> {
            if(startDate.getValue() != null && endDate.getValue() != null)
            {
                String start = startDate.getValue().toString();
                String end = endDate.getValue().toString();
                fillTableWithData(start,end);

            }
        });

    }

    private void registerTables() {
        id.setCellValueFactory(new PropertyValueFactory<DataModel,Long>("id"));
        litir.setCellValueFactory(new PropertyValueFactory<DataModel,String>("litir"));
        fat.setCellValueFactory(new PropertyValueFactory<DataModel,String>("fat"));
        protein.setCellValueFactory(new PropertyValueFactory<DataModel,String>("protein"));
        salt.setCellValueFactory(new PropertyValueFactory<DataModel,String>("salt"));
        water.setCellValueFactory(new PropertyValueFactory<DataModel,String>("water"));
        totalPrice.setCellValueFactory(new PropertyValueFactory<DataModel,String>("totalSumma"));
        sellDate.setCellValueFactory(new PropertyValueFactory<DataModel,String>("purchaseDate"));
    //    state.setCellValueFactory(new PropertyValueFactory<DataModel,String>("state"));
        litrPrice.setCellValueFactory(new PropertyValueFactory<DataModel,String>("litrPrice"));
        paidMoney.setCellValueFactory(new PropertyValueFactory<DataModel,String>("paidMoney"));
        needToPay.setCellValueFactory(new PropertyValueFactory<DataModel,String>("needToPay"));
        table.getStylesheets().add(getClass().getResource("/css/styles.css").toExternalForm());
    }
    public void setTableToShow(SuppModel suppModel){
        this.suppModel = suppModel;
        this.sellerId = suppModel.getDbId();
        fermaName.setText(suppModel.getCompanyName() +" " + suppModel.getDirectorName());
        sellerInfo.setText(suppModel.getAddress()+"  " + suppModel.getPhone());
        table.getItems().addAll( Main.ctx.getBean(HistoryService.class).getLimited(this.sellerId,suppModel.getDirectorName()));
        BigDecimal bigDecimal = Main.ctx.getBean(HistoryService.class).calculateLitir(this.sellerId);
        if(bigDecimal == null)
            totalL.setText("0");
        else
            totalL.setText(bigDecimal.toString());
        BigDecimal bigDecimal1 = Main.ctx.getBean(HistoryService.class).calculateTotalPrice(this.sellerId);
        DecimalFormat decimalFormat = new DecimalFormat("0");
        if(bigDecimal1 == null)
            totalP.setText("0 "+" cўм");
        else
            totalP.setText(decimalFormat.format(bigDecimal1) + " сўм");
        Long payed = Main.ctx.getBean(PayMilkService.class).payedMoney(this.sellerId);
        paid.setText(payed+"  cўм");
        Long totalDebt = Main.ctx.getBean(HistoryService.class).allDebt(this.sellerId);
        debt.setText(totalDebt - payed+" cўм");
    }
    private void fillTableWithData(String start , String end){
        table.getItems().clear();
        table.getItems().addAll( Main.ctx.getBean(HistoryService.class).getHistoryByDate(this.sellerId,suppModel.getDirectorName(),start,end));
    }
}

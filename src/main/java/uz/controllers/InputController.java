package uz.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import uz.Main;
import uz.controllers.controllerEvents.IncomDataEvent;
import uz.controllers.controllerEvents.UpdateBaseEvent;
import uz.controllers.tableData.UpdateData;
import uz.dto.HistoryDto;
import uz.entities.Status;
import uz.entities.TypeSeller;
import uz.service.HistoryService;
import uz.service.PayfundService;
import uz.service.SellerService;
import uz.utils.FxmlLoader;
import uz.utils.HelpUtils;
import uz.utils.Window;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

public class InputController   implements Initializable  {
    @FXML
    private JFXTextField fatField;
    @FXML
    private JFXTextField proteinField;
    @FXML
    private JFXTextField saltField;
    @FXML
    private JFXTextField waterField;
    @FXML
    private JFXTextField totalLitr;
    @FXML
    private JFXButton btnAdd;
    @FXML
    private JFXButton btnCancel;
    @FXML
    private Label milkSupplyer;
    private Long id;
    @FXML
    private Label fill1;
    @FXML
    private Label fill2;
    @FXML
    private Label fill3;
    @FXML
    private Label fill4;
    @FXML
    private Label fill5;
    @FXML
    private Label litrPrice;
    @FXML
    private AnchorPane pane;
    @FXML
    private JFXComboBox<String> base;
    @FXML
    private Button add;
    @FXML
    private Label totalPrice;
    String basePrice = "";
    private HistoryDto historyDto = new HistoryDto();
    @FXML
    private JFXCheckBox payed;
    @FXML
    private JFXCheckBox forDebt;
    @FXML
    private JFXTextField purchaseField;
    private DecimalFormat format = new DecimalFormat("0.##");
    private DecimalFormat format1 = new DecimalFormat("0");
    private TypeSeller typeSeller = TypeSeller.aholi;
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        /*
        Formula to calculate how much it will be ?
        (fat * protein) / (10.8) * bazis * totalLitr
         */
        prepares();
        registerEvents();
        escapeFromWrong();
        basePrice = Main.ctx.getBean(SellerService.class).getBase(this.typeSeller);
        base.getItems().add(basePrice);
        base.getSelectionModel().selectLast();
        forDebt.setDisable(true);
    }


    private void prepares() {
        fill1.setVisible(false);
        fill2.setVisible(false);
        fill3.setVisible(false);
        fill4.setVisible(false);
        fill5.setVisible(false);
        new HelpUtils().isDoubleValidation(fatField);
        new HelpUtils().isDoubleValidation(proteinField);
        new HelpUtils().isDoubleValidation(saltField);
        new HelpUtils().isDoubleValidation(waterField);
        new HelpUtils().isDoubleValidation(totalLitr);
        base.getStylesheets().add(getClass().getResource("/css/styles.css").toExternalForm());
        totalPrice.setText("0");
        litrPrice.setText("0");
    }

    private void registerEvents()
    {
        //After adding new base price
        Main.eventBus.addEventHandler(UpdateBaseEvent.ANY,event -> {
            regenerateBase();
        });
        add.setOnAction(event ->
        {
            Window window = new Window("", FxmlLoader.mainScreen.basisView);
            window.setStageStyle(StageStyle.UNDECORATED);
            AddBaseController base = window.getController();
            base.setTypeSeller(this.typeSeller);
            window.show();
        });
        btnAdd.setOnAction(event ->
        {
            fill1.setVisible(totalLitr.getText().isEmpty());
            fill2.setVisible(fatField.getText().isEmpty());
            fill3.setVisible(proteinField.getText().isEmpty());
            fill4.setVisible(waterField.getText().isEmpty());
            fill5.setVisible(saltField.getText().isEmpty());
            if(checkAll())
            {
                if(     (purchaseField.getText().isEmpty() && forDebt.isSelected()) ||
                        (enoguphMoney(Long.parseLong(purchaseField.getText()),event) && payed.isSelected())
                        || (enoguphMoney(Long.parseLong(purchaseField.getText()),event) && forDebt.isSelected() && !purchaseField.getText().isEmpty())
                )
                {
                this.historyDto.setFat(new BigDecimal(fatField.getText()));
                this.historyDto.setProtein(new BigDecimal(proteinField.getText()));
                this.historyDto.setLitir(new BigDecimal(totalLitr.getText()));
                this.historyDto.setSalt(new BigDecimal(saltField.getText()));
                this.historyDto.setWater(new BigDecimal(waterField.getText()));
                this.historyDto.setDayOrNight(Main.dayOrNight);
                if(!purchaseField.getText().isEmpty())
                    this.historyDto.setPaidMoney(Long.parseLong(purchaseField.getText()));
                else
                    this.historyDto.setPaidMoney((long)0);
                String total = totalPrice.getText().replace(',','.');

                this.historyDto.setSellerId(this.id);
                // update data prosta bosh menyudagi danniylani yangilash uchun garak bazadan so`rovsiz
                UpdateData updateData = new UpdateData();
                if(payed.isSelected())
                {
                     this.historyDto.setStatus(Status.Payed);
                     this.historyDto.setTotalSumma(new BigDecimal("0"));
                     updateData.setPayed(Long.parseLong(purchaseField.getText()));

                }
                else
                    {
                     this.historyDto.setStatus(Status.ForDebt);
                     String temp = "";
                     if(!purchaseField.getText().isEmpty()){
                         temp = format1.format(Double.parseDouble(totalPrice.getText()) - Double.parseDouble(purchaseField.getText())).replace(',','.');
                         updateData.setPayed(Long.parseLong(purchaseField.getText()));
                     }
                     else
                         temp = format1.format(Double.parseDouble(totalPrice.getText())).replace(',','.');
                     this.historyDto.setTotalSumma(new BigDecimal(temp));
                     updateData.setDebt(Long.parseLong(temp));

                }
                updateData.setMilk(new BigDecimal(totalLitr.getText()));
                Main.ctx.getBean(HistoryService.class).createInfo(this.historyDto);
                IncomDataEvent incomDataEvent = new IncomDataEvent(IncomDataEvent.ANY,updateData);
                Main.eventBus.fireEvent(incomDataEvent);
                close(event);
                }
            }
        });
        btnCancel.setOnAction(event -> {
            close(event);
        });
        pane.setOnKeyPressed(event -> {
            if(event.getCode().equals(KeyCode.ENTER)){
                controlFields();
            }
        });
        forDebt.setOnAction(event -> {
            calculation();
            forDebt.setSelected(true);
            payed.setSelected(false);
            purchaseField.setText("");
            purchaseField.requestFocus();
            purchaseField.setDisable(false);

        });
        payed.setOnAction(event -> {
            calculation();
            payed.setSelected(true);
            forDebt.setSelected(false);
            String  temp = format1.format(Double.parseDouble(totalPrice.getText())).replace(',','.');
            purchaseField.setText(temp);
            purchaseField.setDisable(true);

        });
    }



    private boolean checkAll() {
        boolean check = true;
        check &= (!fill1.isVisible());
        check &= (!fill2.isVisible());
        check &= (!fill3.isVisible());
        check &= (!fill4.isVisible());
        check &= (!fill5.isVisible());
        if(payed.isSelected() || forDebt.isSelected())
            return check;
        else
            return false;
    }

    private void close(ActionEvent event) {
            Stage stage = (Stage)((Button)event.getSource()).getScene().getWindow();
            stage.close();
    }

    public void setMilkSupplyer(String milkSupplyer,Long id,TypeSeller typeSeller)
    {
        this.milkSupplyer.setText(milkSupplyer);
        this.id  = id;
        if(typeSeller.equals(TypeSeller.fermer)){
                this.typeSeller = typeSeller.fermer;
                forDebt.setDisable(false);
        }
        regenerateBase();
    }
    private void controlFields()
    {
        if(totalLitr.getText().isEmpty()){
            totalLitr.requestFocus();
        }
        if(fatField.getText().isEmpty()){
            fatField.requestFocus();
        }
        else if(proteinField.getText().isEmpty()){
            proteinField.requestFocus();
        }
        else if(waterField.getText().isEmpty()){
            waterField.requestFocus();
        }
        else if(saltField.getText().isEmpty()){
            saltField.requestFocus();
        }
        else
            {
            calculation();
        }

    }
    private void calculation()
    {
        /*
        Formula to calculate how much it will be ?
        (fat * protein) / (10.8) * bazis * totalLitr
         */
        double result = (Double.parseDouble(fatField.getText()) * (Double.parseDouble(proteinField.getText())))/(10.8);
        litrPrice.setText(format.format(result*Double.parseDouble(base.getSelectionModel().getSelectedItem())).replace(',','.'));
        result = result * (Double.parseDouble(totalLitr.getText())) * (Double.parseDouble(base.getSelectionModel().getSelectedItem()));
        String s = format.format(result).replace(',','.');
        totalPrice.setText(s);
    }
    private void escapeFromWrong() {
        proteinField.textProperty().addListener((observable, oldValue, newValue) -> {
            if(oldValue != newValue){
                makeItFalse();
            }

        });
        totalLitr.textProperty().addListener((observable, oldValue, newValue) -> {
            if(oldValue != newValue){
                makeItFalse();
            }
        });
        fatField.textProperty().addListener((observable, oldValue, newValue) -> {
            if(oldValue != newValue){
                makeItFalse();
            }
        });
    }
    private void makeItFalse(){
        payed.setSelected(false);
        forDebt.setSelected(false);
    }
    private void  regenerateBase(){
        base.getItems().clear();
        basePrice = Main.ctx.getBean(SellerService.class).getBase(this.typeSeller);
        base.getItems().add(basePrice);
        base.getSelectionModel().selectLast();
    }
    private boolean enoguphMoney(Long money, ActionEvent event)
    {
        boolean checkIt = Main.ctx.getBean(PayfundService.class).enoughMoney(money);
        if(!checkIt){
            Tooltip tooltip = new Tooltip();
            tooltip.setText("Кассада етарли пул йўк");
            tooltip.setAutoHide(true);
            Stage stage = (Stage)((Button)event.getSource()).getScene().getWindow();
            tooltip.show(stage);
        }
        return checkIt;
    }

}

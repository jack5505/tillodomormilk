package uz.controllers;

import com.jfoenix.controls.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import uz.Main;
import uz.dto.UserDto;
import uz.entities.UserType;
import uz.service.UserService;
import uz.utils.FxmlLoader;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class LoginController  implements Initializable {
    @FXML
    private Label passError;
    @FXML
    private Button btnExit;
    @FXML
    private JFXButton btnLogin;
    @FXML
    private JFXComboBox<UserType> checkUser;
    @FXML
    private JFXPasswordField passText;
    @FXML
    private ImageView img;
    @FXML
    private JFXTextField login;
    @FXML
    private AnchorPane pane;
    private List<UserDto> list;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        Main.ctx.getBean(UserService.class).createUserIfNotExisted();
        checkUser.getItems().add(UserType.Kunduzgi);
        checkUser.getItems().add(UserType.Tungi);
        this.list = Main.ctx.getBean(UserService.class).getUsers();
        btnLogin.setOnAction(event -> {
            checkIt();
      });
      btnExit.setOnAction(event -> {
          Stage stage = (Stage)((Button)(event.getSource())).getScene().getWindow();
          stage.close();
      });
      checkUser.valueProperty().addListener((observable, oldValue, newValue) -> {
          if(oldValue != newValue){
              if(newValue.equals(UserType.Kunduzgi)){
                  // 1 bo`lsa kun
                    img.setImage(new Image("/img/icons8-солнце-50.png"));
                    Main.dayOrNight = true;
              }
              if(newValue.equals(UserType.Tungi)){
                    img.setImage(new Image("/img/icons8-частичная-облачность-ночью-100.png"));
                    // 0 bo`lsa tun
                    Main.dayOrNight = false;
              }
          }
      });
            pane.setOnKeyPressed(event -> {
                if(event.getCode().equals(KeyCode.ENTER)){
                            checkIt();
                }
            });

    }
    private void checkIt(){
        int ok=0;
        String name = "";
        Long id = (long)0;
        for (int i = 0; i < list.size(); i++) {

            if(list.get(i).getPassword().equals(String.valueOf(passText.getText()))
                    && !checkUser.getSelectionModel().isEmpty()
                    && list.get(i).getLogin().equals(login.getText()))
            {
                ok=1;
                name = list.get(i).getFio();
                id = list.get(i).getId();
            }
        }
        if (ok==1) {
            try {
                FXMLLoader login = new FXMLLoader(getClass().getClassLoader().getResource(FxmlLoader.mainScreen.main));
                Scene scene = new Scene(login.load());
                Main.window.hide();
                Stage stage = new Stage();
                stage.getIcons().add(new Image("/ico/tilloDomor.jpg"));
                stage.setTitle("Tillo domor(" + this.login.getText() + ")");
                stage.setScene(scene);
                //  stage.resizableProperty().setValue(false);
                Controller cnt = login.getController();
                cnt.setUserType(checkUser.getValue(),name,id);
                stage.setMaximized(true);
                stage.setResizable(true);
                stage.setMinWidth(stage.getWidth());
                stage.setMinHeight(stage.getHeight());
                stage.show();
                Main.window = stage;
            } catch (IOException e1) {

            }
        }
        else{
            passError.setVisible(true);
        }
    }


}

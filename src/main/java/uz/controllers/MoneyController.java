package uz.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;
import org.springframework.format.annotation.DateTimeFormat;
import uz.Main;
import uz.controllers.controllerEvents.AddMoneyEvent;
import uz.dto.PayFundDto;
import uz.dto.PayFundTimesDto;
import uz.service.PayFundTimesService;
import uz.service.PayfundService;
import uz.utils.HelpUtils;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

public class MoneyController implements Initializable {
    @FXML
    private JFXTextField money;
    @FXML
    private JFXButton btnAdd;
    @FXML
    private JFXButton btnCancel;
    private PayFundDto payFundDto=new PayFundDto();
    private PayFundTimesDto payFundTimesDto=new PayFundTimesDto();

    private String time;
   private DateTimeFormatter timeFormat= DateTimeFormatter.ofPattern("HH:mm");

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        prepares();
        btnAdd.setOnAction(event -> {
            time=LocalTime.now().format(timeFormat);
            payFundDto.setSum(Long.parseLong(money.getText()) );
            payFundTimesDto.setSum(money.getText());
            payFundTimesDto.setTime(time);

            Main.ctx.getBean(PayfundService.class).create(payFundDto);
            Long payFundId=Main.ctx.getBean(PayfundService.class).getFundsMaxId();
            payFundTimesDto.setPayFundId(payFundId);
            Main.ctx.getBean(PayFundTimesService.class).save(payFundTimesDto);
            AddMoneyEvent addMoneyEvent = new AddMoneyEvent(AddMoneyEvent.ANY);
            Main.eventBus.fireEvent(addMoneyEvent);
            Stage stage = (Stage) btnAdd.getScene().getWindow();
            stage.close();
        });
        btnCancel.setOnAction(event -> {
            Stage stage = (Stage) btnAdd.getScene().getWindow();
            stage.close();
        });
    }


    private void prepares() {
        new HelpUtils().setNumeric(money);
    }
}

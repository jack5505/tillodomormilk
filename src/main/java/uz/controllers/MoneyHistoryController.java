package uz.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.util.StringConverter;
import uz.Main;
import uz.controllers.tableData.HistoryModel;
import uz.controllers.tableData.MoneyHistoryModel;
import uz.controllers.tableData.PayFundTimeModel;
import uz.controllers.tableData.SuppModel;
import uz.service.PayFundTimesService;
import uz.service.PayfundService;
import uz.service.SellerService;
import uz.service.impl.PayFundTimesServiceImpl;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ResourceBundle;
public class MoneyHistoryController implements Initializable
{
    @FXML
    private TableView<PayFundTimeModel> payFundTable;
    @FXML
    private TableColumn<PayFundTimeModel,Long> payFundId;
    @FXML
    private TableColumn<PayFundTimeModel,String> payFundSum;
    @FXML
    private TableColumn<PayFundTimeModel,String> payFundTime;
    @FXML
    private JFXDatePicker startDate;

    @FXML
    private JFXDatePicker endDate;

    @FXML
    private JFXButton search;

    @FXML
    private TableView<MoneyHistoryModel> tableView;
    @FXML
    private TableColumn<MoneyHistoryModel,Long> id;
    @FXML
    private TableColumn<MoneyHistoryModel,Long> olSum;
    @FXML
    private TableColumn<MoneyHistoryModel,Long> qoSum;
    @FXML
    private TableColumn<MoneyHistoryModel,String> date;

    @Override
    public void initialize(URL location, ResourceBundle resources) {


        tableRegister();

        String pattern = "yyyy-MM-dd";

        startDate.setConverter(new StringConverter<LocalDate>() {
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });
        endDate.setConverter(new StringConverter<LocalDate>() {
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });
        search.setOnAction(event -> {
            fillData(startDate.getValue().toString(),endDate.getValue().toString());
        });



        tableView.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                System.out.println("Table");
                Long id=tableView.getSelectionModel().getSelectedItem().getDbId();
                payFundTableRegister(id);
                System.out.println(id);
            }
        });



    }

    private void tableRegister() {
        id.setCellValueFactory(new PropertyValueFactory<MoneyHistoryModel,Long>("id"));
        olSum.setCellValueFactory(new PropertyValueFactory<MoneyHistoryModel,Long>("olSum"));
        qoSum.setCellValueFactory(new PropertyValueFactory<MoneyHistoryModel,Long>("qoSum"));
        date.setCellValueFactory(new PropertyValueFactory<MoneyHistoryModel,String>("date"));

        payFundId.setCellValueFactory(new PropertyValueFactory<PayFundTimeModel,Long>("id"));
        payFundSum.setCellValueFactory(new PropertyValueFactory<PayFundTimeModel,String>("sum"));
        payFundTime.setCellValueFactory(new PropertyValueFactory<PayFundTimeModel,String>("time"));

        payFundTable.getStylesheets().add(getClass().getResource("/css/styles.css").toExternalForm());
        tableView.getStylesheets().add(getClass().getResource("/css/styles.css").toExternalForm());

    }
     private void payFundTableRegister(Long payFundId){
        payFundTable.getItems().clear();
        payFundTable.getItems().addAll(Main.ctx.getBean(PayFundTimesService.class).getPayFundTimeById(payFundId));
      }
     private void fillData(String date1,String date2){
        tableView.getItems().clear();
        tableView.getItems().addAll(Main.ctx.getBean(PayfundService.class).getPayFund(date1,date2));
//        List<MoneyHistoryModel> moneyList=Main.ctx.getBean(PayfundService.class).getPayFund(date1,date2);
     }
}

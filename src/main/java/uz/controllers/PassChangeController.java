package uz.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXPasswordField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import uz.Main;
import uz.dto.UserDto;
import uz.entities.UserEntity;
import uz.entities.UserType;
import uz.repository.SellerRepository;
import uz.repository.UserRepository;
import uz.service.UserService;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class PassChangeController implements Initializable {

    @FXML
    private Label passError;
    @FXML
    private JFXButton BtnLogin;
    @FXML
    private  JFXPasswordField PassText ;
    @FXML
    private JFXPasswordField NewPassText;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        BtnLogin.setOnAction(event -> {
            List<UserDto> list=Main.ctx.getBean(UserService.class).getUsers();
            int ok=0;
            Long userId = null;
            for (int i = 0; i < list.size(); i++) {
                if(list.get(i).getPassword().equals( String.valueOf(PassText.getText())) ){
                    ok=1;
                    userId=list.get(i).getId();
                }
            }
            if (ok==1) {
                UserDto userDto=new UserDto();
                userDto.setId(userId);
                userDto.setPassword(NewPassText.getText());
                Main.ctx.getBean(UserService.class).save(userDto);
                Stage stage = (Stage) ((Button)event.getSource()).getScene().getWindow();
                stage.close();
            }
            else{
                passError.setVisible(true);
            }
        });
    }
}

package uz.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import uz.Main;
import uz.controllers.tableData.MilkPayModel;
import uz.service.HistoryService;
import uz.service.PayMilkService;

import java.math.BigDecimal;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.List;
import java.util.ResourceBundle;

public class TransferHistoryController implements Initializable
{
    @FXML
    private JFXDatePicker startDate;
    @FXML
    private JFXDatePicker endDate;
    @FXML
    private JFXButton search;
    @FXML
    private Label totalL;
    @FXML
    private Label totalP;
    @FXML
    private Label paid;

    @FXML
    private TableView<MilkPayModel> table;

    @FXML
    private TableColumn<MilkPayModel,Long> id;

    @FXML
    private TableColumn<MilkPayModel,String> name;

    @FXML
    private TableColumn<MilkPayModel,String> directorName;

    @FXML
    private TableColumn<MilkPayModel,String> sellDate;

    @FXML
    private TableColumn<MilkPayModel,String> payment;

    @FXML
    private TableColumn<MilkPayModel,String > phone;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        prepareTables();
        fillData();
        prepareClicks();

    }

    private void prepareClicks() {
        search.setOnAction(event ->
        {
            if(startDate.getValue() != null && endDate.getValue() != null)
            {
                table.getItems().clear();
                List<MilkPayModel> list = Main.ctx.getBean(PayMilkService.class).
                        getByDates(startDate.getValue().toString(),endDate.getValue().toString());
                final Long[] total = {(long) 0};
                list.forEach(milkPayModel -> {
                    total[0] += Long.parseLong(milkPayModel.getPaidSumma());
                });
                paid.setText(total[0]+" Сўм");
                table.getItems().addAll(list);
            }

        });
    }

    private void fillData() {
        if(!table.getItems().isEmpty())
            table.getItems().clear();
        List<MilkPayModel> list = Main.ctx.getBean(PayMilkService.class).getAll();
        final Long[] total = {(long) 0};
        list.forEach(milkPayModel -> {
            total[0] += Long.parseLong(milkPayModel.getPaidSumma());
        });
        paid.setText(total[0]+" Сўм");
        table.getItems().addAll(list);
        Long big = Main.ctx.getBean(HistoryService.class).calculateAllDebt();
        totalP.setText(big + " сўм");

    }

    private void prepareTables() {
        id.setCellValueFactory(new PropertyValueFactory<MilkPayModel,Long>("id"));
        name.setCellValueFactory(new PropertyValueFactory<MilkPayModel,String>("companyName"));
        directorName.setCellValueFactory(new PropertyValueFactory<MilkPayModel,String>("directorName"));
        sellDate.setCellValueFactory(new PropertyValueFactory<MilkPayModel,String>("date"));
        phone.setCellValueFactory(new PropertyValueFactory<MilkPayModel,String>("phone"));
        payment.setCellValueFactory(new PropertyValueFactory<MilkPayModel,String>("paidSumma"));
        table.getStylesheets().add(getClass().getResource("/css/styles.css").toExternalForm());
    }
}

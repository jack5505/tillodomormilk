package uz.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import uz.Main;
import uz.controllers.controllerEvents.PayMoneyEvent;
import uz.controllers.tableData.SuppModel;
import uz.dto.HistoryDto;
import uz.dto.PayMilkDto;
import uz.dto.UpdateHistoryDto;
import uz.entities.Status;
import uz.service.HistoryService;
import uz.service.PayMilkService;
import uz.service.SellerService;
import uz.utils.HelpUtils;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class TransferMoneyController implements Initializable {
    @FXML
    private JFXComboBox<SuppModel> farms;
    @FXML
    private JFXTextField money;
    @FXML
    private DatePicker date;
    @FXML
    private JFXButton btnAdd;
    @FXML
    private JFXButton btnCancel;
    private SuppModel suppModel;
    @FXML
    private Label info1;

    @FXML
    private Label info2;
    @FXML
    private Label info3;
    @FXML
    private Label shouldPay;
    @FXML
    private JFXCheckBox cash;
    @FXML
    private JFXCheckBox transfer;
    @FXML
    private Label date1;
    @FXML
    private ImageView dateImg;


    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        prepares();
        registerClicks();
    }

    private void registerClicks()
    {
        btnAdd.setOnAction(event ->
        {
            info1.setVisible(farms.getSelectionModel().getSelectedItem() == null);
            info2.setVisible(money.getText().isEmpty());
            if(cash.isSelected() || (!cash.isSelected() && !transfer.isSelected()))
                info3.setVisible(date.getValue() == null ? true : false);
            if(transfer.isSelected() || cash.isSelected())
            {
                if(transfer.isSelected())
                {
                    //Transfer money
                if(!info1.isVisible() && !info2.isVisible() && !info3.isVisible())
                {
                    PayMilkDto payMilkDto =
                            new PayMilkDto(date.getValue().toString(),
                                    Long.parseLong(money.getText()),
                                    farms.getSelectionModel().getSelectedItem().getDbId());
                    savePayed(payMilkDto);
                    close(event);
                }
                }
                else
                    {
                    info1.setVisible(farms.getSelectionModel().getSelectedItem() == null);
                    info2.setVisible(money.getText().isEmpty());
                    if(!info1.isVisible() && !info2.isVisible())
                    {
                        //Nalichka to`lasa Cash payed
                        UpdateHistoryDto updateHistoryDto =
                                new UpdateHistoryDto(farms.getSelectionModel().getSelectedItem().getDbId(),
                                LocalDate.now().toString(),
                                Long.parseLong(money.getText()));
                                Main.ctx.getBean(HistoryService.class).payMoney(updateHistoryDto);
                                Main.eventBus.fireEvent(new PayMoneyEvent(PayMoneyEvent.ANY));
                        close(event);
                    }
                }

            }
        });
        btnCancel.setOnAction(event -> {
            Stage stage = (Stage)((Button)event.getSource()).getScene().getWindow();
            stage.close();
        });
        farms.valueProperty().addListener((observable, oldValue, newValue) ->
        {
            if(oldValue != newValue){
                shouldPay.setText(Main.ctx.getBean(SellerService.class).debtOfFarms(newValue.getDbId())+" Сўм");
            }

        });
        cash.setOnAction(event -> {
            info1.setVisible(farms.getSelectionModel().getSelectedItem() == null);
            info2.setVisible(money.getText().isEmpty());
            if(!info1.isVisible() && !info2.isVisible())
                btnAdd.setDisable(false);
            cash.setSelected(true);
            transfer.setSelected(false);
            cash.setStyle("-fx-text-fill:#3404f2");
            transfer.setStyle("-fx-text-fill:black ");
            info3.setVisible(false);
            dissapear(false);
        });
        transfer.setOnAction(event -> {
            info1.setVisible(farms.getSelectionModel().getSelectedItem() == null);
            info2.setVisible(money.getText().isEmpty());
            info3.setVisible(date.getValue() == null ? true : false);
            if(!info1.isVisible() && !info2.isVisible() && !info3.isVisible())
                btnAdd.setDisable(false);
            cash.setSelected(false);
            transfer.setSelected(true);
            transfer.setStyle("-fx-text-fill:red");
            cash.setStyle("-fx-text-fill:black ");
            dissapear(true);
        });
        money.textProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.isEmpty()){
                info2.setVisible(true);
                btnAdd.setDisable(true);
            }
            if(!newValue.isEmpty())
                info2.setVisible(false);
            if(!info2.isVisible() && farms.getSelectionModel().getSelectedItem() != null){
                if(transfer.isSelected() && date.getValue() != null){
                    btnAdd.setDisable(false);
                }
                if(cash.isSelected()){
                    btnAdd.setDisable(false);
                }

            }
        });

        date.getEditor().textProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.isEmpty()){
                info3.setVisible(true);
                btnAdd.setDisable(true);
            }
            if(!newValue.isEmpty())
                info3.setVisible(false);
            if(!newValue.isEmpty() && !money.getText().isEmpty() && farms.getSelectionModel().getSelectedItem() != null){
                btnAdd.setDisable(false);
                info3.setVisible(false);
            }
        });


    }

    private void savePayed(PayMilkDto payMilkDto) {
        Main.ctx.getBean(PayMilkService.class).saveIt(payMilkDto);
    }

    private void close(ActionEvent event) {
        Stage stage = (Stage)((Button)event.getSource()).getScene().getWindow();
        stage.close();
    }

    private void dissapear(boolean b) {
      date.setVisible(b);
      dateImg.setVisible(b);
      date1.setVisible(b);

    }

    private void prepares() {
        new HelpUtils().setNumeric(money);
        btnAdd.setDisable(true);
        farms.getItems().addAll(Main.ctx.getBean(SellerService.class).getAllSuppData());
        info1.setVisible(false);
        info2.setVisible(false);
        info3.setVisible(false);
        shouldPay.setText("0 Сўм");
    }
    public void setSupplyer(SuppModel suppModel){
            this.suppModel = suppModel;

            farms.getItems().forEach(suppModel1 -> {
                    if(suppModel1.getDbId().equals(this.suppModel.getDbId())){
                        farms.getSelectionModel().select(suppModel1);
                    }
            });
    }
}

package uz.controllers.controllerEvents;

import javafx.beans.NamedArg;
import javafx.event.Event;
import javafx.event.EventType;

public class AddDataEvent extends Event
{
    public static final EventType<AddDataEvent> ANY =
            new EventType<>(Event.ANY, "Add_PRODUCT_CART");
    public AddDataEvent(@NamedArg("eventType") EventType<? extends Event> eventType) {
        super(eventType);
    }

}

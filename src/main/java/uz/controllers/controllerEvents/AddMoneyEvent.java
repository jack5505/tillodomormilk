package uz.controllers.controllerEvents;

import javafx.beans.NamedArg;
import javafx.event.Event;
import javafx.event.EventType;

public class AddMoneyEvent extends Event {
    public static final EventType<AddMoneyEvent> ANY =
            new EventType<>(Event.ANY, "Add_MONEY_EVENT");
    public AddMoneyEvent(@NamedArg("eventType") EventType<? extends Event> eventType) {
        super(eventType);
    }
}

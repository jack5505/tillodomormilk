package uz.controllers.controllerEvents;

import javafx.beans.NamedArg;
import javafx.event.Event;
import javafx.event.EventType;
import uz.controllers.tableData.UpdateData;

public class IncomDataEvent extends Event
{
    private UpdateData updateData;
    public static final EventType<IncomDataEvent> ANY =
            new EventType<>(Event.ANY, "ADD_INCOME_DATA");
    public IncomDataEvent(@NamedArg("eventType") EventType<? extends Event> eventType,UpdateData updateData) {
        super(eventType);
        this.updateData = updateData;
    }

    public UpdateData getUpdateData() {
        return updateData;
    }

    public void setUpdateData(UpdateData updateData) {
        this.updateData = updateData;
    }
}

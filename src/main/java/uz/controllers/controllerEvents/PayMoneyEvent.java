package uz.controllers.controllerEvents;

import javafx.beans.NamedArg;
import javafx.event.Event;
import javafx.event.EventType;

public class PayMoneyEvent extends Event {
    public static final EventType<PayMoneyEvent> ANY =
            new EventType<>(Event.ANY,"PAYED_CASH");
    public PayMoneyEvent(@NamedArg("eventType")EventType<? extends Event> eventType){
        super(eventType);
    }

}

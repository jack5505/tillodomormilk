package uz.controllers.controllerEvents;

import javafx.beans.NamedArg;
import javafx.event.Event;
import javafx.event.EventType;

public class UpdateBaseEvent extends Event {
    public static final EventType<UpdateBaseEvent> ANY =
            new EventType<>(Event.ANY, "UPDATE_BASE_DIGIT");
    public UpdateBaseEvent(@NamedArg("eventType") EventType<? extends Event> eventType) {
        super(eventType);
    }
}

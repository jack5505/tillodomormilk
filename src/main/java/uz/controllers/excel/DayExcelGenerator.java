package uz.controllers.excel;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import uz.Main;
import uz.controllers.tableData.HistoryModel;
import uz.service.HistoryService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class DayExcelGenerator extends ExcelTemplate {
    private static  int start = 2;
    @Override
    public void generatedExcel(int type,String date) {
        // 1 - type kunduzi
        // 2 - type kechqurun
        //3 - type kuni bo`yi
        List<HistoryModel> list = new ArrayList<>();
        if(type == 1)
             list = Main.ctx.getBean(HistoryService.class).todaysReport(true);
        else if(type == 2)
            list = Main.ctx.getBean(HistoryService.class).todaysReport(false);
        else{
            list = Main.ctx.getBean(HistoryService.class).getAllHistoryByDate(date);
        }
        String titleOfSheet = "Тарихи";
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(titleOfSheet);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        LocalDateTime now = LocalDateTime.now();
        Row headerRow = sheet.createRow(0);
        Cell cell = headerRow.createCell(0);
        if(type == 1)
            cell.setCellValue("Таминотчилар тарихи  кундузги  сана:" + date);
        else if(type == 2)
            cell.setCellValue("Таминотчилар тарихи  кечкурунги  сана:" + date);
        else
            cell.setCellValue("Таминотчилар тарихи  куни бўйи  сана:" + date);
        sheet.addMergedRegion(CellRangeAddress.valueOf("$A$1:$I$1"));
        String[] column = {"Таминотчилар тури","Таминотчилар номи","Жами литр","Каймоки","Оксили",
                "Тузи","Суви","Жами нарх","Сотиб олинган вакти","Тўлов холати",
                "Сут нархи","Тўланган нарх","Тўлаши керак бўлган нарх"};
        headerRow = sheet.createRow(1);
        for(int i = 0 ; i < column.length ;i ++){
            Cell cell1 = headerRow.createCell(i);
            cell1.setCellValue(column[i]);
        }
        start = 2;
        list.forEach(historyModel ->
        {
            Row row = sheet.createRow(start);
            improveRow(0,historyModel.getSuppName(),row);
            improveRow(1,historyModel.getFio(),row);
            improveRow(2,historyModel.getTotalLittr(),row);
            improveRow(3,historyModel.getFatH(),row);
            improveRow(4,historyModel.getProteinH(),row);
            improveRow(5,historyModel.getSaltH(),row);
            improveRow(6,historyModel.getWaterH(),row);
            improveRow(7,historyModel.getTotalSumm(),row);
            improveRow(8,historyModel.getPurchaseDate(),row);
            improveRow(9,historyModel.getStatusPay(),row);
            improveRow(10,historyModel.getMilkCost(),row);
            improveRow(11,historyModel.getPaidMoney(),row);
            improveRow(12,historyModel.getNeedToPay(),row);
            start ++;
        });


        openExcel(workbook);

    }

    @Override
    public String getFileName() {
        return "Эрталабки сут кабул"+new Date().getTime();
    }
}

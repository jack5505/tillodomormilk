package uz.controllers.excel;

import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import uz.controllers.tableData.HistoryModel;


import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public abstract class ExcelTemplate {
    protected void openExcel(XSSFWorkbook workbook){
        try {
            DirectoryChooser dirChooser = new DirectoryChooser();
            File path = new File(System.getProperty("user.home"));
            dirChooser.setInitialDirectory(path);
            dirChooser.setTitle("Саклаш учун папкани танланг");
            Stage stage  = new Stage();

            File myFile = new File(dirChooser.showDialog(stage)+ "/" + getFileName() + ".xlsx");
            myFile.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(myFile);
            workbook.write(outputStream);
            workbook.close();
            Desktop desktop = Desktop.getDesktop();
            if (myFile.exists()) desktop.open(myFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    protected void improveRow(int index, String text, Row row){
        Cell cell = row.createCell(index);
        cell.setCellValue(text);
    }

    public abstract void generatedExcel(int type,String date);

    public abstract String getFileName();

}

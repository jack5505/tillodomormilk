package uz.controllers.loading;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import uz.Main;
import uz.config.AppConfig;
import uz.config.PersistenceConfig;
import uz.utils.FxmlLoader;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoadingController implements Initializable {

    private Timeline timeline;


    @FXML
    ProgressBar progressBar;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        Thread thread = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                Main.ctx = new AnnotationConfigApplicationContext();
                Main.ctx.register(AppConfig.class);
                Main.ctx.register(PersistenceConfig.class);
                Main.ctx.refresh();
            }
        });
        thread.setDaemon(true);
        thread.start();
        IntegerProperty seconds = new SimpleIntegerProperty();
        progressBar.progressProperty().bind(seconds.divide(90.0));
        timeline = new Timeline(
                new KeyFrame(Duration.ZERO, new KeyValue(seconds, 0)),
                new KeyFrame(Duration.seconds(10), e-> {
                    try {
                        FXMLLoader login = new FXMLLoader(getClass().getClassLoader().getResource(FxmlLoader.loading.userScreen));
                        Scene scene = new Scene(login.load());
                        Main.window.hide();
                        Stage stage = new Stage();
                        stage.getIcons().add(new Image("/ico/tilloDomor.jpg"));
                        stage.setScene(scene);
                      //  stage.resizableProperty().setValue(false);
                        stage.setResizable(true);
                        stage.initStyle(StageStyle.UNDECORATED);
                        stage.show();
                        Main.window = stage;
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    timeline.stop();
                }, new KeyValue(seconds, 100))
        );
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();


    }
}

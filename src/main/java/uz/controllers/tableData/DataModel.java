package uz.controllers.tableData;

import uz.dto.HistoryDto;

public class DataModel extends HistoryDto {
    private String name;
    private String sell;
    private String state;
    private String litrPrice;
    private String needToPay;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLitrPrice() {
        return litrPrice;
    }

    public void setLitrPrice(String litrPrice) {
        this.litrPrice = litrPrice;
    }


    public String getNeedToPay() {
        return needToPay;
    }

    public void setNeedToPay(String needToPay) {
        this.needToPay = needToPay;
    }
}

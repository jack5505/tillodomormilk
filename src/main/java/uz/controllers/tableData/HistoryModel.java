package uz.controllers.tableData;

public class HistoryModel {
    private Long id;
    private String suppName;
    private String fio;
    private String paidMoney;
    private String totalSumm;
    private String totalLittr;
    private String milkCost;
    private String statusPay;
    private String fatH;
    private String proteinH;
    private String saltH;
    private String waterH;
    private String purchaseDate;
    private String needToPay;
    public String getMilkCost() {
        return milkCost;
    }

    public void setMilkCost(String milkCost) {
        this.milkCost = milkCost;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPaidMoney() {
        return paidMoney;
    }

    public void setPaidMoney(String paidMoney) {
        this.paidMoney = paidMoney;
    }

    public String getTotalSumm() {
        return totalSumm;
    }

    public void setTotalSumm(String totalSumm) {
        this.totalSumm = totalSumm;
    }

    public String getTotalLittr() {
        return totalLittr;
    }

    public void setTotalLittr(String totalLittr) {
        this.totalLittr = totalLittr;
    }

    public String getStatusPay() {
        return statusPay;
    }

    public void setStatusPay(String statusPay) {
        this.statusPay = statusPay;
    }

    public String getSuppName() {
        return suppName;
    }

    public void setSuppName(String suppName) {
        this.suppName = suppName;
    }

    public String getFatH() {
        return fatH;
    }

    public void setFatH(String fatH) {
        this.fatH = fatH;
    }

    public String getProteinH() {
        return proteinH;
    }

    public void setProteinH(String proteinH) {
        this.proteinH = proteinH;
    }

    public String getSaltH() {
        return saltH;
    }

    public void setSaltH(String saltH) {
        this.saltH = saltH;
    }

    public String getWaterH() {
        return waterH;
    }

    public void setWaterH(String waterH) {
        this.waterH = waterH;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getNeedToPay() {
        return needToPay;
    }

    public void setNeedToPay(String needToPay) {
        this.needToPay = needToPay;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }
}

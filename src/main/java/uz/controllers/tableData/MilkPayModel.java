package uz.controllers.tableData;

public class MilkPayModel {
    private Long id;
    private String directorName;
    private String companyName;
    private String phone;
    private String date;
    private String paidSumma;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDirectorName() {
        return directorName;
    }

    public void setDirectorName(String directorName) {
        this.directorName = directorName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPaidSumma() {
        return paidSumma;
    }

    public void setPaidSumma(String paidSumma) {
        this.paidSumma = paidSumma;
    }
}

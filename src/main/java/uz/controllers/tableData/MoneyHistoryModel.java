package uz.controllers.tableData;

public class MoneyHistoryModel {
    private Long id;
    private Long olSum;
    private Long qoSum;
    private String date;
    private Long dbId;

    public MoneyHistoryModel() {
    }

    public Long getDbId() {
        return dbId;
    }

    public MoneyHistoryModel(Long id, Long olSum, Long qoSum, String date, Long dbId) {
        this.id = id;
        this.olSum = olSum;
        this.qoSum = qoSum;
        this.date = date;
        this.dbId = dbId;
    }

    public void setDbId(Long dbId) {
        this.dbId = dbId;
    }

    public MoneyHistoryModel(Long id, Long olSum, Long qoSum, String date) {
        this.id = id;
        this.olSum = olSum;
        this.qoSum = qoSum;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOlSum() {
        return olSum;
    }

    public void setOlSum(Long olSum) {
        this.olSum = olSum;
    }

    public Long getQoSum() {
        return qoSum;
    }

    public void setQoSum(Long qoSum) {
        this.qoSum = qoSum;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}

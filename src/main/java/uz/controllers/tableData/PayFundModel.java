package uz.controllers.tableData;

import java.util.Date;

public class PayFundModel {
    private Long id;
    private Long sum;
    private String date;

    public PayFundModel() {
    }

    public PayFundModel(Long id, Long sum, String date) {
        this.id = id;
        this.sum = sum;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSum() {
        return sum;
    }

    public void setSum(Long sum) {
        this.sum = sum;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}

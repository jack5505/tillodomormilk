package uz.controllers.tableData;

public class PayFundTimeModel {
    private Long id;
    private String sum;
    private String time;

    public PayFundTimeModel() {
    }

    public PayFundTimeModel(Long id, String sum, String time) {
        this.id = id;
        this.sum = sum;
        this.time = time;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}

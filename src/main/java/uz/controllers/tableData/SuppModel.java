package uz.controllers.tableData;

import uz.entities.TypeSeller;

public class SuppModel {
    private Long id;
    private String companyName;
    private String directorName;
    private String phone;
    private String address;
    private Long dbId;
    private TypeSeller typeSeller;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDirectorName() {
        return directorName;
    }

    public void setDirectorName(String directorName) {
        this.directorName = directorName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getDbId() {
        return dbId;
    }

    public void setDbId(Long dbId) {
        this.dbId = dbId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return  companyName+directorName;
    }

    public TypeSeller getTypeSeller() {
        return typeSeller;
    }

    public void setTypeSeller(TypeSeller typeSeller) {
        this.typeSeller = typeSeller;
    }
}

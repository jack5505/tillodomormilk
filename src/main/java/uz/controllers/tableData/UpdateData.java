package uz.controllers.tableData;

import java.math.BigDecimal;

public class UpdateData {
    private BigDecimal milk;
    private Long payed = (long)0;
    private Long debt = (long)0;

    public BigDecimal getMilk() {
        return milk;
    }

    public void setMilk(BigDecimal milk) {
        this.milk = milk;
    }

    public Long getPayed() {
        return payed;
    }

    public void setPayed(Long payed) {
        this.payed = payed;
    }

    public Long getDebt() {
        return debt;
    }

    public void setDebt(Long debt) {
        this.debt = debt;
    }
}

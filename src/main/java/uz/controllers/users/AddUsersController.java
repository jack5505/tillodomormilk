package uz.controllers.users;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import uz.Main;
import uz.dto.UserDto;
import uz.service.UserService;

import java.net.URL;
import java.util.ResourceBundle;

public class AddUsersController implements Initializable {
    @FXML
    private JFXTextField fieldName;
    @FXML
    private JFXTextField fieldLogin;
    @FXML
    private JFXTextField fieldPassword;
    @FXML
    private JFXButton btnAdd;
    @FXML
    private JFXButton btnCancel;
    @FXML
    private Label error1;
    @FXML
    private Label error2;
    @FXML
    private Label error3;
    private UserDto userDto;
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        error1.setVisible(false);
        error2.setVisible(false);
        error3.setVisible(false);
        events();


    }

    private void events() {

            btnAdd.setOnAction(event -> {
                checkIt();
                if(!error1.isVisible()
                        && !error2.isVisible()
                        && !error3.isVisible())
                {
                    if(userDto == null)
                            userDto = new UserDto();
                    this.userDto.setFio(fieldName.getText());
                    this.userDto.setPassword(fieldPassword.getText());
                    this.userDto.setLogin(fieldLogin.getText());
                    Main.ctx.getBean(UserService.class).save(userDto);
                    UsersListsController.users.clear();
                    UsersListsController.users.addAll(Main.ctx.getBean(UserService.class).getUsers());
                    Stage stage = (Stage)((Button)event.getSource()).getScene().getWindow();
                    stage.close();
                }
            });
            btnCancel.setOnAction(event -> {
                Stage stage = (Stage)((Button)event.getSource()).getScene().getWindow();
                stage.close();
            });
    }

    private void checkIt() {
        error1.setVisible(fieldName.getText().isEmpty());
        error2.setVisible(fieldLogin.getText().isEmpty());
        error3.setVisible(fieldPassword.getText().isEmpty());

    }

    public void setForEdit(UserDto userDto) {
        this.userDto = userDto;
        fieldName.setText(userDto.getFio());
        fieldLogin.setText(userDto.getLogin());
        fieldPassword.setText(userDto.getPassword());

    }
}

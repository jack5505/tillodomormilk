package uz.controllers.users;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import uz.Main;
import uz.dto.UserDto;
import uz.service.UserService;


import java.net.URL;
import java.util.ResourceBundle;

public class DeleteController implements Initializable
{
    @FXML
    private JFXButton btnAdd;
    @FXML
    private JFXButton btnCancel;
    @FXML
    private Label info;
    private UserDto userDto;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
            btnAdd.setOnAction(event -> {
                UsersListsController.users.clear();
                Main.ctx.getBean(UserService.class).delete(this.userDto);
                UsersListsController.users.addAll(Main.ctx.getBean(UserService.class).getUsers());
                Stage stage = (Stage)((JFXButton)event.getSource()).getScene().getWindow();
                stage.close();
            });
            btnCancel.setOnAction(event -> {
                Stage stage = (Stage)((JFXButton)event.getSource()).getScene().getWindow();
                stage.close();
            });
    }
    public void setInto(UserDto userDto){
        this.userDto = userDto;
        info.setText(userDto.getFio());
    }
}

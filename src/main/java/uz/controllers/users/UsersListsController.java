package uz.controllers.users;

import com.jfoenix.controls.JFXButton;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.StageStyle;
import uz.Main;
import uz.dto.UserDto;
import uz.service.UserService;
import uz.utils.FxmlLoader;
import uz.utils.Window;


import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class UsersListsController implements Initializable {
    @FXML
    private JFXButton add;

    @FXML
    private JFXButton edit;

    @FXML
    private JFXButton delet;

    @FXML
    private TableView<UserDto> table;

    @FXML
    private TableColumn<UserDto,Long> num;

    @FXML
    private TableColumn<UserDto,String> fio;

    @FXML
    private TableColumn<UserDto,String> login;

    @FXML
    private TableColumn<UserDto,String> password;

    public static ObservableList<UserDto> users = FXCollections.observableArrayList();


    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        prepares();
        fillTable();
        events();
        table.setItems(users);
    }

    private void events() {
        add.setOnAction(event -> {
            Window window = new Window("", FxmlLoader.users.add);
            window.setStageStyle(StageStyle.UTILITY);
            window.show();
        });
        edit.setOnAction(event -> {
            if(!table.getSelectionModel().isEmpty()){
                UserDto userDto = table.getSelectionModel().getSelectedItem();
                Window window = new Window("",FxmlLoader.users.add);
                AddUsersController cnt = window.getController();
                cnt.setForEdit(userDto);
                window.show();
            }
        });
        delet.setOnAction(event -> {
            if(!table.getSelectionModel().isEmpty())
            {
            Window window = new Window("",FxmlLoader.users.delete);
            window.setStageStyle(StageStyle.UTILITY);
            DeleteController deleteController = window.getController();
            deleteController.setInto(table.getSelectionModel().getSelectedItem());
            window.show();
            }
        });
    }

    private void fillTable() {
        if(!users.isEmpty())
            users.clear();
        users.addAll(Main.ctx.getBean(UserService.class).getUsers());
    }

    private void prepares() {
        num.setCellValueFactory(new PropertyValueFactory<UserDto,Long>("numId"));
        fio.setCellValueFactory(new PropertyValueFactory<UserDto,String>("fio"));
        login.setCellValueFactory(new PropertyValueFactory<UserDto,String>("login"));
        password.setCellValueFactory(new PropertyValueFactory<UserDto,String>("password"));
        table.getStylesheets().add(getClass().getResource("/css/styles.css").toExternalForm());
    }
}

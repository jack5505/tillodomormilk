package uz.dto;

import uz.entities.Status;

import java.math.BigDecimal;
import java.time.LocalDate;

public class HistoryDto {
    private Long id;
    private BigDecimal water = BigDecimal.ZERO;
    private BigDecimal fat = BigDecimal.ZERO;
    private BigDecimal protein = BigDecimal.ZERO;
    private BigDecimal salt = BigDecimal.ZERO;
    private Long sellerId;
    private Long fundsId;
    private BigDecimal litir = BigDecimal.ZERO;
    private BigDecimal totalSumma = BigDecimal.ZERO;
    private Status status;
    private String purchaseDate = LocalDate.now().toString();
    private Long paidMoney = (long)0;
    private boolean dayOrNight;
    public Long getId() {
        return id;
    }

    public Long getFundsId() {
        return fundsId;
    }

    public void setFundsId(Long fundsId) {
        this.fundsId = fundsId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getWater() {
        return water;
    }

    public void setWater(BigDecimal water) {
        this.water = water;
    }

    public BigDecimal getFat() {
        return fat;
    }

    public void setFat(BigDecimal fat) {
        this.fat = fat;
    }

    public BigDecimal getProtein() {
        return protein;
    }

    public void setProtein(BigDecimal protein) {
        this.protein = protein;
    }

    public BigDecimal getSalt() {
        return salt;
    }

    public void setSalt(BigDecimal salt) {
        this.salt = salt;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public BigDecimal getLitir() {
        return litir;
    }

    public void setLitir(BigDecimal litir) {
        this.litir = litir;
    }

    public BigDecimal getTotalSumma() {
        return totalSumma;
    }

    public void setTotalSumma(BigDecimal totalSumma) {
        this.totalSumma = totalSumma;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public Long getPaidMoney() {
        return paidMoney;
    }

    public void setPaidMoney(Long paidMoney) {
        this.paidMoney = paidMoney;
    }

    public HistoryDto(Long sellerId,Long paidMoney,Status status) {
        this.sellerId = sellerId;
        this.paidMoney = paidMoney;
        this.status = status;
    }

    public HistoryDto() {
    }

    public boolean isDayOrNight() {
        return dayOrNight;
    }

    public void setDayOrNight(boolean dayOrNight) {
        this.dayOrNight = dayOrNight;
    }
}

package uz.dto;

import uz.entities.Status;

import java.math.BigDecimal;
import java.time.LocalDate;
public class PayFundDto {
    private Long id;
    private Long sum;
    private String date = LocalDate.now().toString();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSum() {
        return sum;
    }

    public void setSum(Long sum) {
        this.sum = sum;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}

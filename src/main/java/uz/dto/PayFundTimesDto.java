package uz.dto;

public class PayFundTimesDto {
    private Long id;
    private String sum;
    private String time;
    private Long payFundId;

    public PayFundTimesDto() {
    }

    public PayFundTimesDto(Long id, String sum, String time, Long payFundId) {
        this.id = id;
        this.sum = sum;
        this.time = time;
        this.payFundId = payFundId;
    }

    public Long getPayFundId() {
        return payFundId;
    }

    public void setPayFundId(Long payFundId) {
        this.payFundId = payFundId;
    }

    public PayFundTimesDto(Long id, String sum, String time) {
        this.id = id;
        this.sum = sum;
        this.time = time;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}

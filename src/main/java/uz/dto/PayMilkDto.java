package uz.dto;

public class PayMilkDto {
    private Long id;
    private String payedDate;
    private Long payedSumma;
    private Long sellerId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPayedDate() {
        return payedDate;
    }

    public void setPayedDate(String payedDate) {
        this.payedDate = payedDate;
    }

    public Long getPayedSumma() {
        return payedSumma;
    }

    public void setPayedSumma(Long payedSumma) {
        this.payedSumma = payedSumma;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public PayMilkDto(String payedDate, Long payedSumma, Long sellerId) {
        this.payedDate = payedDate;
        this.payedSumma = payedSumma;
        this.sellerId = sellerId;
    }
}

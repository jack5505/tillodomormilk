package uz.dto;

import uz.entities.TypeSeller;

public class SellerDto
{
    private Long id;
    private String companyName;
    private String director;
    private String phone;
    private String address;
    private boolean deleted = false;
    private TypeSeller sellerType;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public TypeSeller getSellerType() {
        return sellerType;
    }

    public void setSellerType(TypeSeller sellerType) {
        this.sellerType = sellerType;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return "SellerDto{" +
                "id=" + id +
                ", companyName='" + companyName + '\'' +
                ", director='" + director + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}

package uz.dto;

//This class use when we pay cash money to supplyer
public class UpdateHistoryDto {
    private Long id;
    private String date;
    private Long summa;

    public UpdateHistoryDto(Long id, String date, Long summa) {
        this.id = id;
        this.date = date;
        this.summa = summa;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getSumma() {
        return summa;
    }

    public void setSumma(Long summa) {
        this.summa = summa;
    }
}

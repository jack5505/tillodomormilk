package uz.entities;

import javax.persistence.*;

@Entity(name = "base")
public class BaseEntity {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private Long baseSumma;

    @Enumerated(EnumType.STRING)
    private TypeSeller typeSeller;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBaseSumma() {
        return baseSumma;
    }

    public void setBaseSumma(Long baseSumma) {
        this.baseSumma = baseSumma;
    }

    public TypeSeller getTypeSeller() {
        return typeSeller;
    }

    public void setTypeSeller(TypeSeller typeSeller) {
        this.typeSeller = typeSeller;
    }
}

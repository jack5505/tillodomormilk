package uz.entities;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity(name = "history")
public class HistoryEntity
{
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private BigDecimal water;

    @Column
    private BigDecimal fat;

    @Column
    private BigDecimal protein;

    @Column
    private BigDecimal salt;

    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToOne
    @JoinColumn(name = "sellerId",insertable = false,updatable = false)
    private SellerEntity seller;

    @Column(name = "sellerId")
    private Long sellerId;
    @ManyToOne
    @JoinColumn(name = "fundsId",insertable = false,updatable = false)
    private PayFundEntity funds;

    @ManyToOne
    @JoinColumn(name = "userId",insertable = false,updatable = false)
    private UserEntity userEntity;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "fundsId")
    private Long fundsId;

    @Column
    private Long paidMoney;

    @Column(name = "dayOrNight")
    private boolean dayOrNight;

    public PayFundEntity getFunds() {
        return funds;
    }

    public void setFunds(PayFundEntity funds) {
        this.funds = funds;
    }

    public Long getFundsId() {
        return fundsId;
    }

    public void setFundsId(Long fundsId) {
        this.fundsId = fundsId;
    }

    @Column
    private BigDecimal totalSumma;

    @Column
    private BigDecimal litir;

    @Column
    private String purchaseDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getWater() {
        return water;
    }

    public void setWater(BigDecimal water) {
        this.water = water;
    }

    public BigDecimal getFat() {
        return fat;
    }

    public void setFat(BigDecimal fat) {
        this.fat = fat;
    }

    public BigDecimal getProtein() {
        return protein;
    }

    public void setProtein(BigDecimal protein) {
        this.protein = protein;
    }

    public BigDecimal getSalt() {
        return salt;
    }

    public void setSalt(BigDecimal salt) {
        this.salt = salt;
    }

    public SellerEntity getSeller() {
        return seller;
    }

    public void setSeller(SellerEntity seller) {
        this.seller = seller;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public BigDecimal getTotalSumma() {
        return totalSumma;
    }

    public void setTotalSumma(BigDecimal totalSumma) {
        this.totalSumma = totalSumma;
    }

    public BigDecimal getLitir() {
        return litir;
    }

    public void setLitir(BigDecimal litir) {
        this.litir = litir;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public Long getPaidMoney() {
        return paidMoney;
    }

    public void setPaidMoney(Long paidMoney) {
        this.paidMoney = paidMoney;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public boolean isDayOrNight() {
        return dayOrNight;
    }

    public void setDayOrNight(boolean dayOrNight) {
        this.dayOrNight = dayOrNight;
    }
}

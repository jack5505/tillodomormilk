package uz.entities;

import javax.persistence.*;

@Entity(name = "funds")
public class PayFundEntity {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private Long sum;
    @Column
    private String date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSum() {
        return sum;
    }

    public void setSum(Long sum) {
        this.sum = sum;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}

package uz.entities;

import javax.persistence.*;

@Entity(name = "payFundTimes")
public class PayFundTimesEntity {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private Long sum;

    @Column
    private String time;

    @ManyToOne
    @JoinColumn(name = "payFundId",insertable = false,updatable = false)
    private PayFundEntity payfund;

    @Column
    private Long payFundId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSum() {
        return sum;
    }

    public void setSum(Long sum) {
        this.sum = sum;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public PayFundEntity getPayfund() {
        return payfund;
    }

    public void setPayfund(PayFundEntity payfund) {
        this.payfund = payfund;
    }

    public Long getPayFundId() {
        return payFundId;
    }

    public void setPayFundId(Long payFundId) {
        this.payFundId = payFundId;
    }
}

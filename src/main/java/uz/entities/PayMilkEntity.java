package uz.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import javax.persistence.*;

@Entity(name = "PayMilk")
public class PayMilkEntity
{
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String payedDate;

    @Column
    private Long payedSumma;

    @ManyToOne
    @JoinColumn(name = "sellerId",insertable = false,updatable = false)
    private SellerEntity seller;

    @Column(name = "sellerId")
    private Long sellerId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPayedDate() {
        return payedDate;
    }

    public void setPayedDate(String payedDate) {
        this.payedDate = payedDate;
    }

    public Long getPayedSumma() {
        return payedSumma;
    }

    public void setPayedSumma(Long payedSumma) {
        this.payedSumma = payedSumma;
    }

    public SellerEntity getSeller() {
        return seller;
    }

    public void setSeller(SellerEntity seller) {
        this.seller = seller;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }
}

package uz.entities;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity(name = "seller")
public class SellerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String companyName;

    @Column
    private String director;

    @Column
    private String phone;

    @Column
    private String address;

    @Column
    private boolean deleted;

    @Enumerated(EnumType.STRING)
    private TypeSeller sellerType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public TypeSeller getSellerType() {
        return sellerType;
    }

    public void setSellerType(TypeSeller sellerType) {
        this.sellerType = sellerType;
    }
}

package uz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.entities.BaseEntity;

public interface BaseRepository  extends JpaRepository<BaseEntity,Long> {
    /*
    Base Service inside of SellerService
     */
    @Query(value = "SELECT baseSumma FROM base WHERE typeSeller=:type ORDER BY id DESC LIMIT 1",nativeQuery = true)
    Long getLast(@Param("type") String type);
    @Query(value = "SELECT COUNT(id) FROM base",nativeQuery = true)
    Long check();
    @Query(value = "SELECT * FROM base  WHERE typeSeller =:type LIMIT 1",nativeQuery = true)
    BaseEntity getFirst(@Param("type")String type);

}

package uz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.entities.HistoryEntity;

import java.math.BigDecimal;
import java.util.List;

public interface HistoryRepository extends JpaRepository<HistoryEntity,Long>
{
    @Query(value = "select * from history where purchaseDate=CURDATE() and dayOrNight = :dayOrNight",nativeQuery = true)
    List<HistoryEntity> todaysHistory(@Param("dayOrNight") boolean dayOrNight);
    @Query(value = "SELECT * FROM history WHERE sellerid = :sellerId ORDER BY id DESC LIMIT 30",nativeQuery = true)
    List<HistoryEntity> getLast30(@Param("sellerId") Long sellerId);
    @Query(value = "SELECT SUM(litir) FROM history WHERE sellerId = :sellerId",nativeQuery = true)
    BigDecimal calculateLitir(@Param("sellerId") Long sellerId);
    @Query(value = "SELECT SUM(totalSumma) + sum(paidMoney) FROM history WHERE sellerId = :sellerId ",nativeQuery = true)
    BigDecimal calculateSumma(@Param("sellerId") Long sellerId);
    @Query(value = "SELECT SUM(litir) FROM history WHERE purchaseDate = CURDATE()",nativeQuery = true)
    BigDecimal todaysTotalLitir();
    @Query(value = "SELECT sum(totalSumma) FROM history WHERE purchaseDate = CURDATE() && STATUS = 'Payed'",nativeQuery = true)
    Long todaysTotalPayedSumma();
    @Query(value = "SELECT SUM(totalSumma) FROM history WHERE purchaseDate = CURDATE() && STATUS = 'ForDebt'",nativeQuery = true)
    Long todaysTotalDebtSumma();
    @Query(value = "SELECT * FROM history WHERE sellerId = :sellerId AND purchaseDate >= :start AND purchaseDate <= :end",nativeQuery = true)
    List<HistoryEntity> historyByDate(@Param("sellerId") Long sellerId,@Param("start") String start,@Param("end")String end);
    @Query(value = "SELECT SUM(totalSumma) FROM history WHERE sellerId = :sellerId AND purchaseDate >= :start AND purchaseDate <= :end && STATUS = 'Payed'",nativeQuery = true)
    BigDecimal historyTotalPayedSumma(@Param("sellerId")Long sellerId,@Param("start") String start, @Param("end") String end);
    @Query(value = "SELECT SUM(totalSumma) FROM history WHERE sellerId = :sellerId AND purchaseDate >= :start AND purchaseDate <= :end && STATUS = 'ForDebt'",nativeQuery = true)
    BigDecimal historyTotalDebtSumma(@Param("sellerId")Long sellerId,@Param("start") String start, @Param("end") String end);
    @Query(value = "SELECT SUM(totalSumma) FROM history WHERE sellerId = :sellerId AND purchaseDate >= :start AND purchaseDate <= :end",nativeQuery = true)
    BigDecimal historyTotalSumma(@Param("sellerId")Long sellerId,@Param("start") String start, @Param("end") String end);
    @Query(value = "select  sum(totalSumma) from history where sellerId = :sellerId and status = 'ForDebt'",nativeQuery = true)
    Long totalDebt(@Param("sellerId")Long sellerId);
    @Query(value = "SELECT SUM(totalSumma) FROM history WHERE STATUS = 'ForDebt'",nativeQuery = true)
    Long totalDebtAll();


    @Query(value = "SELECT sum(totalSumma) FROM history WHERE STATUS = 'Payed' AND fundsId = :fundsId AND purchaseDate >= :start AND purchaseDate <= :end",nativeQuery = true)
    Long  sumHistoryByDates(@Param("fundsId") Long fundsId, @Param("start") String start, @Param("end") String end);
    @Query(value = "SELECT sum(paidMoney) from history where purchaseDate = curdate()",nativeQuery = true)
    Long totalTodaysPaidSumma();
    @Query(value = "select * from history where purchaseDate = :searchDate",nativeQuery = true)
    List<HistoryEntity> getByDate(@Param("searchDate") String searchDate);

    @Query(value = "select * from history where purchaseDate =:date and sellerId = :id limit 1",nativeQuery = true)
    HistoryEntity payedCash(@Param("id") Long id, @Param("date") String date);

}

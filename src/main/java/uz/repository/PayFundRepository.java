package uz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.entities.HistoryEntity;
import uz.entities.PayFundEntity;

import java.util.Date;
import java.util.List;

public interface PayFundRepository extends JpaRepository <PayFundEntity,Long>{

//    @Query("select a from Article a where a.creationDateTime <= :creationDateTime")

    @Query(value = "SELECT SUM(`sum`) FROM funds WHERE DATE = :date",nativeQuery = true)
    Long todaysCash(@Param("date")String date);
    @Query(value = "SELECT `id` FROM funds WHERE DATE = :date",nativeQuery = true)
    Long count(@Param("date")String date);

    @Query(value = "SELECT MAX(id) FROM funds ",nativeQuery = true)
    Long maxId();
}

package uz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uz.controllers.tableData.PayFundTimeModel;
import uz.entities.PayFundTimesEntity;

import java.util.List;

public interface PayFundTimesRepository extends JpaRepository<PayFundTimesEntity,Long> {



    @Query(value = "SELECT * FROM payfundtimes WHERE payFundId=:payFundId",nativeQuery = true)
     List<PayFundTimesEntity> findByFundId(@Param("payFundId") Long payFundId);
}

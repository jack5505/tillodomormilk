package uz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.entities.PayMilkEntity;

import java.util.List;

public interface PayMilkRepository extends JpaRepository<PayMilkEntity,Long> {
    @Query(value = "SELECT * FROM paymilk WHERE payedDate = CURDATE()",nativeQuery = true)
    List<PayMilkEntity> todayAll();
    @Query(value = "SELECT SUM(payedSumma) FROM paymilk WHERE sellerId = :sellerId",nativeQuery = true)
    Long totalPayedMoney(@Param("sellerId")Long sellerId);
    @Query(value = "SELECT * FROM paymilk WHERE payedDate >= :start AND payedDate <= :end",nativeQuery = true)
    List<PayMilkEntity> historyByDates(@Param("start")String start, @Param("end")String end);
    @Query(value = "SELECT SUM(payedSumma) FROM paymilk",nativeQuery = true)
    Long allPayedMoney();

}

package uz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.entities.SellerEntity;

public interface SellerRepository extends JpaRepository<SellerEntity,Long> {
    @Query(value = "SELECT COUNT(id) FROM seller where deleted = false",nativeQuery = true)
    Long totalCnt();
    @Query(value = "SELECT SUM(totalSumma) FROM history WHERE sellerId = :sellerId",nativeQuery = true)
    Long totalDebt(@Param("sellerId") Long sellerId);
}

package uz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.entities.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity,Long> {

}

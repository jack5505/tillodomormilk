package uz.service;

import uz.controllers.tableData.DataModel;
import uz.controllers.tableData.HistoryModel;
import uz.dto.HistoryDto;
import uz.dto.UpdateHistoryDto;

import java.math.BigDecimal;
import java.util.List;

public interface HistoryService {
    void createInfo(HistoryDto historyDto);
    List<HistoryModel> todaysReport(boolean dayOrNight);
    List<DataModel> getLimited(Long id,String name);
    BigDecimal calculateLitir(Long id);
    BigDecimal calculateTotalPrice(Long id);
    BigDecimal todayLitir();
    Long todayPayed();
    Long todayDebt();
    List<DataModel> getHistoryByDate(Long sellerId, String directorName,String start,String end);
    Long allDebt(Long sellerId);
    Long calculateAllDebt();
    void payedItCash(HistoryDto historyDto);
    List<HistoryModel> getAllHistoryByDate(String searchDate);

    void payMoney(UpdateHistoryDto updateHistoryDto);
}

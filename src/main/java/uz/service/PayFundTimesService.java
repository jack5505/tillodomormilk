package uz.service;

import uz.controllers.tableData.PayFundTimeModel;
import uz.dto.PayFundTimesDto;
import uz.entities.PayFundTimesEntity;

import java.util.List;

public interface PayFundTimesService {

    List<PayFundTimeModel> getPayFundTimeById(Long payFundId);

    void save(PayFundTimesDto payFundTimesDto);

}
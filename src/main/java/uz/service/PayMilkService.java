package uz.service;

import uz.controllers.tableData.MilkPayModel;
import uz.dto.PayMilkDto;

import java.util.List;

public interface PayMilkService {
    List<MilkPayModel> getAll();
    void saveIt(PayMilkDto payMilkDto);
    Long payedMoney(Long sellerId);
    List<MilkPayModel> getByDates(String date1 , String date2);
}

package uz.service;

import uz.controllers.tableData.MoneyHistoryModel;
import uz.dto.PayFundDto;

import java.util.List;

public interface PayfundService {
     void create(PayFundDto payFundDto);
     List<MoneyHistoryModel> getPayFund(String date1, String date2);
     Long getCashMoney(String date);
     Long getFundsMaxId();
     boolean enoughMoney(Long money);

}

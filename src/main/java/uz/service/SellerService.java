package uz.service;

import uz.controllers.tableData.SuppModel;
import uz.dto.SellerDto;
import uz.entities.TypeSeller;

import java.util.List;

public interface SellerService {
    void saveSeller(SellerDto sellerDto);
    SellerDto getSellerById(Long id);
    List<SuppModel> getAllSuppData();
    void deletSeller(Long id);
    String getBase(TypeSeller typeSeller);
    void saveBase(String temp,TypeSeller type);
    void checkIfNotExisted();
    Long debtOfFarms(Long sellerId);
    Long getAllSeller();
    void updateBase(Long baseSumma,TypeSeller typeSeller);
}

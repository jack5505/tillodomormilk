package uz.service;

import uz.dto.UserDto;

import java.util.List;

public interface UserService {

    void save(UserDto userDto);
    List<UserDto> getUsers();
    void createUserIfNotExisted();

    void delete(UserDto userDto);
}

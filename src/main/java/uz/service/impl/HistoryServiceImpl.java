package uz.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.Main;
import uz.controllers.tableData.DataModel;
import uz.controllers.tableData.HistoryModel;
import uz.dto.HistoryDto;
import uz.dto.UpdateHistoryDto;
import uz.entities.HistoryEntity;
import uz.entities.SellerEntity;
import uz.entities.Status;
import uz.entities.TypeSeller;
import uz.repository.HistoryRepository;
import uz.repository.PayFundRepository;
import uz.repository.PayMilkRepository;
import uz.repository.SellerRepository;
import uz.service.HistoryService;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


@Service
public class HistoryServiceImpl implements HistoryService
{

    private DecimalFormat format=new DecimalFormat("0.#");

    @Autowired
    private HistoryRepository historyRepository;
    @Autowired
    private SellerRepository sellerRepository;
    @Autowired
    private PayMilkRepository payMilkRepository;
    @Autowired
    private PayFundRepository payFundRepository;
    private  HashMap<Long,String> aholi;
    private HashMap<Long,String> fermer;

    @Override
    public void createInfo(HistoryDto historyDto) {
        HistoryEntity historyEntity = new HistoryEntity();
        Long id = payFundRepository.count(historyDto.getPurchaseDate());
        historyDto.setFundsId(id);
        BeanUtils.copyProperties(historyDto, historyEntity);
        historyRepository.save(historyEntity);
    }

    @Override
    public List<HistoryModel> todaysReport(boolean dayOrNight) {
        //1 bo`lsa kun
        //0 bo`lsan tun
        findIt();
        List<HistoryEntity> historyEntities = historyRepository.todaysHistory(dayOrNight);
        boolean alltype = false;
        return  fillItForMe(historyEntities,alltype);
    }

    private List<HistoryModel> fillItForMe(List<HistoryEntity> historyEntities,boolean alltype) {
        List<HistoryModel> list = new ArrayList<>();
        historyEntities.forEach(historyEntity -> {
            HistoryModel historyModel = new HistoryModel();
            historyModel.setId((long) (list.size() + 1));
            historyModel.setPaidMoney(historyEntity.getPaidMoney().toString());
            if(historyEntity.getSellerId() == null){
                historyModel.setFio("Ахоли");
                historyModel.setSuppName("Ахоли");
            }
            else{
                if(fermer.containsKey(historyEntity.getSellerId())){
                    historyModel.setSuppName("Фермер");
                    historyModel.setFio(fermer.get(historyEntity.getSellerId()));
                }
                else{
                    historyModel.setFio(aholi.get(historyEntity.getSellerId()));
                    historyModel.setSuppName("Ахоли");
                }
            }
            double sum=historyEntity.getTotalSumma().doubleValue()+historyEntity.getPaidMoney().doubleValue();
            double litr=historyEntity.getLitir().doubleValue();
            historyModel.setFatH(format.format(historyEntity.getFat()));
            historyModel.setProteinH(format.format(historyEntity.getProtein()));
            historyModel.setWaterH(format.format(historyEntity.getWater()));
            historyModel.setSaltH(format.format(historyEntity.getSalt()));
            historyModel.setTotalLittr(format.format(historyEntity.getLitir()));
            if(litr == 0){
            historyModel.setMilkCost( format.format("0"));
            historyModel.setTotalSumm(format.format("0"));
            }else {
                historyModel.setMilkCost( format.format(sum/litr));
                historyModel.setTotalSumm(format.format(sum));
            }
            if(alltype == false)
                historyModel.setPurchaseDate(historyEntity.getPurchaseDate());
            else{
                    if(historyEntity.isDayOrNight()){
                        historyModel.setPurchaseDate("Эрталаб");
                    }
                    else{
                        historyModel.setPurchaseDate("Кечкурун");
                    }
            }
            historyModel.setNeedToPay(format.format(historyEntity.getTotalSumma()));
//            if(historyEntity.getStatus().equals(Status.Payed))
//                historyModel.setStatusPay("ОК");
//            else
//                historyModel.setStatusPay("Карзмиз");
            list.add(historyModel);
        });
        return list;
    }

    @Override
    public List<DataModel> getLimited(Long id,String name)
    {
        List<HistoryEntity> list = historyRepository.getLast30(id);
        return this.fillHistoryModel(list,name);
    }

    @Override
    public BigDecimal calculateLitir(Long id)
    {
        BigDecimal rt = historyRepository.calculateLitir(id);
        return rt;
    }

    @Override
    public BigDecimal calculateTotalPrice(Long id) {
        BigDecimal rt = historyRepository.calculateSumma(id);
        return rt;
    }

    @Override
    public BigDecimal todayLitir() {
        BigDecimal rt = historyRepository.todaysTotalLitir();
            if(rt == null)
                return BigDecimal.ZERO;
        return rt;
    }

    @Override
    public Long todayPayed() {
        Long rt = historyRepository.totalTodaysPaidSumma();
        if(rt == null)
            return  (long) 0;
        return rt;
    }

    @Override
    public Long todayDebt() {
        Long rt = historyRepository.todaysTotalDebtSumma();
        if(rt == null)
            return (long) 0;
        return rt;
    }

    @Override
    public List<DataModel> getHistoryByDate(Long sellerId, String directorName,String start, String end) {
        List<HistoryEntity> listHistory = historyRepository.historyByDate(sellerId,start,end);
        return this.fillHistoryModel(listHistory,directorName);
    }

    @Override
    public Long allDebt(Long sellerId)
    {
        Long len = historyRepository.totalDebt(sellerId);
        if(len == null)
            return (long)0;
        return len;
    }

    @Override
    public Long calculateAllDebt()
    {
        Long first = historyRepository.totalDebtAll();
        if(first == null)
            first = (long)0;
        Long second = payMilkRepository.allPayedMoney();
        if(second == null)
            second = (long)0;
        return (first - second);
    }

    @Override
    public void payedItCash(HistoryDto historyDto)
    {
        Long id = payFundRepository.count(historyDto.getPurchaseDate());
    }

    @Override
    public List<HistoryModel> getAllHistoryByDate(String searchDate) {
        findIt();
        List<HistoryEntity> historyEntities = historyRepository.getByDate(searchDate);
        boolean alltype = true;
        List<HistoryModel> list =  fillItForMe(historyEntities,alltype);
        return list;
    }

    @Override
    public void payMoney(UpdateHistoryDto updateHistoryDto)
    {

        HistoryEntity historyEntity = historyRepository.payedCash(updateHistoryDto.getId(),updateHistoryDto.getDate());
        if(historyEntity == null){
                historyEntity = new HistoryEntity();
                historyEntity.setPaidMoney(updateHistoryDto.getSumma());
                historyEntity.setTotalSumma(new BigDecimal("0"));
                historyEntity.setSalt(new BigDecimal("0"));
                historyEntity.setFat(new BigDecimal("0"));
                historyEntity.setLitir(new BigDecimal("0"));
                historyEntity.setProtein(new BigDecimal("0"));
                historyEntity.setPurchaseDate(updateHistoryDto.getDate());
                historyEntity.setDayOrNight(Main.dayOrNight);
                historyEntity.setWater(new BigDecimal("0"));
                historyEntity.setTotalSumma(new BigDecimal("0"));
                historyEntity.setSellerId(updateHistoryDto.getId());
                historyEntity.setFundsId(payFundRepository.maxId());
        }
        else{
            historyEntity.setPaidMoney(historyEntity.getPaidMoney() + updateHistoryDto.getSumma());
            historyEntity.setTotalSumma(historyEntity.getTotalSumma().subtract(new BigDecimal(updateHistoryDto.getSumma())));
        }

        historyRepository.save(historyEntity);
    }

    private void findIt() {
        this.aholi = new HashMap<>();
        this.fermer = new HashMap<>();
        List<SellerEntity> sellList = sellerRepository.findAll();
        sellList.parallelStream().forEach(sellerEntity -> {
            if(!fermer.containsKey(sellerEntity.getId()) && sellerEntity.getSellerType().equals(TypeSeller.fermer)){
                fermer.put(sellerEntity.getId(),sellerEntity.getDirector());
            }
            else if(!aholi.containsKey(sellerEntity.getId())){
                aholi.put(sellerEntity.getId(),sellerEntity.getDirector());
            }
        });
    }


    private List<DataModel> fillHistoryModel(List<HistoryEntity> list,String name){
        Locale.setDefault(Locale.US);
        DecimalFormat format = new DecimalFormat("0.##");
        List<DataModel> rt = new ArrayList<>();
        list.forEach(historyEntity ->
        {
            DataModel dataModel = new DataModel();
            dataModel.setId((long) (rt.size() + 1));
            dataModel.setFat(new BigDecimal(format.format(historyEntity.getFat())));
            dataModel.setLitir(new BigDecimal(format.format(historyEntity.getLitir())));
            dataModel.setSalt(new BigDecimal(format.format(historyEntity.getSalt())));
            dataModel.setProtein(new BigDecimal(format.format(historyEntity.getProtein())));
            dataModel.setWater(new BigDecimal(format.format(historyEntity.getWater())));
            if(historyEntity.getStatus().equals(Status.Payed))
            {
                dataModel.setNeedToPay("0");
                dataModel.setState("ОК");
                dataModel.setTotalSumma(new BigDecimal(format.format(historyEntity.getTotalSumma())).add(new BigDecimal(historyEntity.getPaidMoney())));
            }
            else
                {
                dataModel.setNeedToPay(format.format(historyEntity.getTotalSumma()));
                dataModel.setState("Тўланмаган");
                dataModel.setTotalSumma(new BigDecimal(format.format(historyEntity.getTotalSumma())).add(new BigDecimal(historyEntity.getPaidMoney())));
            }
            dataModel.setPaidMoney(historyEntity.getPaidMoney());
            double eachPrice = Double.parseDouble(dataModel.getTotalSumma().toString());
            eachPrice /= Double.parseDouble(dataModel.getLitir().toString());
            dataModel.setLitrPrice(format.format(eachPrice));
            dataModel.setPurchaseDate(historyEntity.getPurchaseDate());
            dataModel.setName(name);
            rt.add(dataModel);
        });
        return  rt;
    }
}

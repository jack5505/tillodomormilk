package uz.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.controllers.tableData.MoneyHistoryModel;
import uz.dto.PayFundDto;
import uz.entities.PayFundEntity;
import uz.repository.HistoryRepository;
import uz.repository.PayFundRepository;
import uz.service.PayfundService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class PayFundServiceImp implements PayfundService {
    @Autowired
    private PayFundRepository payFundRepository;
    @Autowired
    private HistoryRepository historyRepository;
    @Override
    public void create(PayFundDto payFundDto) {
        PayFundEntity payFundEntity=new PayFundEntity();
        Long sum=payFundRepository.todaysCash(payFundDto.getDate());
        Long id=payFundRepository.count(payFundDto.getDate());
        payFundEntity.setId(id);
        if(id==null)
            payFundEntity.setSum(payFundDto.getSum());
        else
            payFundEntity.setSum(sum + payFundDto.getSum());
        payFundEntity.setDate(payFundDto.getDate());
        payFundRepository.save(payFundEntity);
    }

    @Override
    public List<MoneyHistoryModel> getPayFund(String date1, String date2) {
        List<MoneyHistoryModel> moneyHistoryModelList=new ArrayList<>();
        long numer=0;
        List<PayFundEntity> list = payFundRepository.findAll();
        for (int i = 0; i < list.size(); i++) {
            MoneyHistoryModel moneyHistoryModel=new MoneyHistoryModel((long)(i+1),list.get(i).getSum(),null,list.get(i).getDate(),list.get(i).getId());
            Long sum=historyRepository.sumHistoryByDates( list.get(i).getId(), date1 ,date2 );
            if(sum!=null) {
                numer++;
                moneyHistoryModel.setId(numer);
                moneyHistoryModel.setQoSum(moneyHistoryModel.getOlSum()-sum);
                moneyHistoryModelList.add(moneyHistoryModel);
            }
            System.out.println(sum);
        }

        return moneyHistoryModelList;
    }

    @Override
    public Long getCashMoney(String date)
    {
        Long rt = payFundRepository.todaysCash(date);
        if(rt == null)
            return (long)0;
        return rt;
    }

    @Override
    public Long getFundsMaxId() {
        Long maxid = payFundRepository.maxId();
        return maxid;
    }

    @Override
    public boolean enoughMoney(Long money) {
        Long rt = getCashMoney(LocalDate.now().toString());
        Long alreadyPaid = historyRepository.totalTodaysPaidSumma();
        if(rt == null)
            rt = (long)0;
        if(alreadyPaid == null)
            alreadyPaid = (long)0;
        if(rt - alreadyPaid >= money)
            return  true;
        return false;
    }

}

package uz.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.controllers.tableData.PayFundTimeModel;
import uz.dto.PayFundTimesDto;
import uz.entities.PayFundTimesEntity;
import uz.repository.PayFundTimesRepository;
import uz.service.PayFundTimesService;

import java.util.ArrayList;
import java.util.List;


@Service
public class PayFundTimesServiceImpl implements PayFundTimesService {
    @Autowired
    private PayFundTimesRepository payFundTimesRepository;


    @Override
    public List<PayFundTimeModel> getPayFundTimeById(Long payFundId) {
        List<PayFundTimesEntity> byFundId =payFundTimesRepository.findByFundId(payFundId);
        List<PayFundTimeModel> answer=new ArrayList<>();
        for (int i = 0; i < byFundId.size(); i++) {
            PayFundTimeModel payFundTimeModel=new PayFundTimeModel((long)(i+1),byFundId.get(i).getSum().toString(),byFundId.get(i).getTime());
            answer.add(payFundTimeModel);
        }
        return answer;
    }

    @Override
    public  void save(PayFundTimesDto payFundTimesDto){
        PayFundTimesEntity payFundTimesEntity=new PayFundTimesEntity();
        payFundTimesEntity.setSum(Long.parseLong(payFundTimesDto.getSum()));
        payFundTimesEntity.setTime(payFundTimesDto.getTime());
        payFundTimesEntity.setPayFundId(payFundTimesDto.getPayFundId());
        payFundTimesRepository.save(payFundTimesEntity);
    }


}

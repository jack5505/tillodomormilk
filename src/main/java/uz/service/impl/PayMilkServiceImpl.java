package uz.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.controllers.tableData.MilkPayModel;
import uz.dto.PayMilkDto;
import uz.entities.PayMilkEntity;
import uz.entities.SellerEntity;
import uz.repository.PayMilkRepository;
import uz.repository.SellerRepository;
import uz.service.PayMilkService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
public class PayMilkServiceImpl implements PayMilkService
{
    @Autowired
    private PayMilkRepository payMilkRepository;
    @Autowired
    private SellerRepository sellerRepository;

    @Override
    public List<MilkPayModel> getAll()
    {
        List<PayMilkEntity> list = payMilkRepository.todayAll();
        List<MilkPayModel> rt = this.convertIt(list);
        return rt;
    }

    @Override
    public void saveIt(PayMilkDto payMilkDto) {
        PayMilkEntity payMilkEntity = new PayMilkEntity();
        BeanUtils.copyProperties(payMilkDto,payMilkEntity);
        payMilkRepository.save(payMilkEntity);
    }

    @Override
    public Long payedMoney(Long sellerId)
    {

        Long payedMoney = payMilkRepository.totalPayedMoney(sellerId);
        if(payedMoney == null)
            return (long)0;
        return payedMoney;
    }

    @Override
    public List<MilkPayModel> getByDates(String date1, String date2)
    {
        List<PayMilkEntity> list = payMilkRepository.historyByDates(date1,date2);
        return this.convertIt(list);
    }

    private List<MilkPayModel> convertIt(List<PayMilkEntity> list)
    {
            List<MilkPayModel> milkPayModelList = new ArrayList<>();
            list.forEach(payMilkEntity ->
            {
                MilkPayModel milkPayModel = new MilkPayModel();
                milkPayModel.setDate(payMilkEntity.getPayedDate());
                milkPayModel.setPaidSumma(payMilkEntity.getPayedSumma()+"");
                Optional<SellerEntity> sellerEntity = sellerRepository.findById(payMilkEntity.getSellerId());
                if(sellerEntity.isPresent()){
                    SellerEntity sellerEntity1 = sellerEntity.get();
                    milkPayModel.setCompanyName(sellerEntity1.getCompanyName());
                    milkPayModel.setDirectorName(sellerEntity1.getDirector());
                    milkPayModel.setPhone(sellerEntity1.getPhone());
                }
                milkPayModelList.add(milkPayModel);
            });
            return milkPayModelList;
    }
}

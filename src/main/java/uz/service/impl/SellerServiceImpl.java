package uz.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import uz.controllers.tableData.SuppModel;
import uz.dto.SellerDto;
import uz.entities.BaseEntity;
import uz.entities.SellerEntity;
import uz.entities.TypeSeller;
import uz.repository.BaseRepository;
import uz.repository.SellerRepository;
import uz.service.SellerService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SellerServiceImpl implements SellerService
{
    /*
    Base also ins this Service

     */
    @Autowired
    private SellerRepository sellerRepository;
    @Autowired
    private BaseRepository baseRepository;
    @Override
    public void saveSeller(SellerDto sellerDto)
    {
        SellerEntity sellerEntity = new SellerEntity();
        BeanUtils.copyProperties(sellerDto,sellerEntity);
        sellerRepository.save(sellerEntity);
    }

    @Override
    public SellerDto getSellerById(Long id)
    {
        SellerDto sellerDto = new SellerDto();
        Optional<SellerEntity> sellerEntity = sellerRepository.findById(id);
        if(sellerEntity.isPresent()){
            SellerEntity sellerEntity1 = sellerEntity.get();
            BeanUtils.copyProperties(sellerEntity1,sellerDto);
        }
        return sellerDto;
    }

    @Override
    public List<SuppModel> getAllSuppData()
    {
        List<SellerEntity> list = sellerRepository.findAll();
        List<SuppModel> list1 = new ArrayList<>();
        list.forEach(sellerEntity -> {
            if(!sellerEntity.isDeleted())
            {
            SuppModel suppModel = new SuppModel();
            suppModel.setId((long) (list1.size() + 1));
            suppModel.setCompanyName(sellerEntity.getCompanyName());
            suppModel.setDirectorName(sellerEntity.getDirector());
            suppModel.setPhone(sellerEntity.getPhone());
            suppModel.setAddress(sellerEntity.getAddress());
            suppModel.setDbId(sellerEntity.getId());
            suppModel.setTypeSeller(sellerEntity.getSellerType());
            list1.add(suppModel);
            }
        });
        return list1;
    }

    @Override
    public void deletSeller(Long id) {
        Optional<SellerEntity> sellerEntity = sellerRepository.findById(id);
        if(sellerEntity.isPresent()){
            SellerEntity sellerEntity1 = sellerEntity.get();
            sellerEntity1.setDeleted(true);
            sellerRepository.save(sellerEntity1);
        }
    }

    @Override
    public String getBase(TypeSeller typeSeller)
    {
        Long last = baseRepository.getLast(typeSeller.toString());
        if(last == null){
            saveBase("2000",typeSeller);
            Long lt = baseRepository.getLast(typeSeller.toString());
            return lt.toString();
        }
        return last.toString();
    }
    @Override
    public void saveBase(String temp,TypeSeller typeSeller) {
            Long get = Long.parseLong(temp);
            BaseEntity baseEntity = new BaseEntity();
            baseEntity.setBaseSumma(get);
            baseEntity.setTypeSeller(typeSeller);
            baseRepository.save(baseEntity);
    }

    @Override
    public void checkIfNotExisted() {
            Long answer = baseRepository.check();
            if(answer == 0){
                BaseEntity baseEntity = new BaseEntity();
                baseEntity.setBaseSumma((long)2000);
                baseRepository.save(baseEntity);
            }
    }

    @Override
    public Long debtOfFarms(Long sellerId)
    {
        Long cnt = sellerRepository.totalDebt(sellerId);
        if(cnt == null)
            return (long)0;
        return cnt;
    }

    @Override
    public Long getAllSeller()
    {
        Long cnt = sellerRepository.totalCnt();
        return cnt;
    }

    @Override
    public void updateBase(Long baseSumma,TypeSeller typeSeller)
    {
        BaseEntity baseEntity = baseRepository.getFirst(typeSeller.toString());
        baseEntity.setBaseSumma(baseSumma);
        baseRepository.save(baseEntity);
    }

}

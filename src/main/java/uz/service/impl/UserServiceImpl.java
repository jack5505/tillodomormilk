package uz.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.dto.UserDto;
import uz.entities.UserEntity;
import uz.repository.SellerRepository;
import uz.repository.UserRepository;
import uz.service.UserService;

import java.util.ArrayList;
import java.util.List;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;


    @Override
    public void save(UserDto userDto) {
        UserEntity userEntity = new UserEntity();
        BeanUtils.copyProperties(userDto, userEntity);
        userRepository.save(userEntity);
    }

    @Override
    public List<UserDto> getUsers() {
//        List<UserDto> userDtoList=new ArrayList<>();
        List<UserDto> list = new ArrayList<>();
        List<UserEntity> userEntities = userRepository.findAll();
        userEntities.forEach(userEntity -> {
            UserDto userDto = new UserDto();
            userDto.setNumId((long)list.size() + 1);
            userDto.setId(userEntity.getId());
            userDto.setLogin(userEntity.getLogin());
            userDto.setPassword(userEntity.getPassword());
            userDto.setFio(userEntity.getFio());
            list.add(userDto);
        });
        return  list;
    }

    @Override
    public void createUserIfNotExisted() {
        Long answer = userRepository.count();
        if(answer == 0){
            UserEntity userEntity = new UserEntity("admin","admin","Feruzbek");
            userRepository.save(userEntity);
        }
    }

    @Override
    public void delete(UserDto userDto) {
        UserEntity userEntity = new UserEntity();
        BeanUtils.copyProperties(userDto,userEntity);
        userRepository.delete(userEntity);
    }


}

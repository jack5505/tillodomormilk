package uz.utils;

public interface FxmlLoader {
    interface mainScreen
    {
        String main = "views/mainView.fxml";
        String add ="views/addSeller.fxml";
        String delet = "views/deletView.fxml";
        String input = "views/inputView.fxml";
        String moneyIn = "views/moneyView.fxml";
        String historySeller = "views/historySellerView.fxml";
        String historyMoney="views/moneyHistoryView.fxml";
        String basisView = "views/addBaseView.fxml";
        String passChangeScreen ="views/PasswordChange.fxml";
        String hisobotScreen="views/hisobot.fxml";
    }
    interface transferScreen{
        String payTransfer = "views/transferMoneyView.fxml";
    }
    interface  milk{
        String milkPayHistoryScreen = "views/historyPayMilkView.fxml";

    }
    interface  loading{
        String userScreen="views/LoginView.fxml";
        String loadScreen = "views/loadingViews/LoadingScreen.fxml";
    }
    interface  users{
        String add = "views/users/addUsers.fxml";
        String list = "views/users/UsersLists.fxml";
        String delete = "views/users/delet.fxml";
    }

}
